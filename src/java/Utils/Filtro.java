package Utils;

import dao.DAOFactory;
import dao.GrupoDAO;
import dao.UsuarioDAO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Grupo;
import model.Usuario;
import sun.awt.X11.XConstants;

public class Filtro {
    
    public final String HASHTAG = "#";
    public final String IMAGEM = "$i";
    public final String VIDEO = "$v";
    public final String LINK = "$l";
    public final String USUARIO_GRUPO = "@";
    
    public Filtro(){}
    
    public List<String> buscarTags(String texto, String tag){
        List<String> tags = new ArrayList<>();
        if(texto.contains(tag)){
            String[] tokens = texto.split(" ");
            for (String string : tokens) {
                if(string.startsWith(tag)){
                    string = string.replace(tag, "");
                    tags.add(string);
                }
            }
        }
        return tags;  
    }
    
    public String colocarLinkHashTag(String texto, String caminho){
        String textoComLink = "";
        String[] tokens = texto.split(" ");
        for (String string : tokens) {
            if(string.startsWith(HASHTAG)){
                string = string.replace(HASHTAG, "");
                string = "<a href=\""+caminho+"/hashtag?tag="+string+"\">"+"#"+string+"</a>";

            }
            textoComLink = textoComLink.concat(" "+string);
        }
        return textoComLink;
    }
    
    public String tragarTags(String texto, String caminho){
        String textoComLink = "";
        String[] tokens = texto.split(" ");
        
        for (String string : tokens) {
            if(string.startsWith("$i:\"")){
                int fim = string.lastIndexOf("\"");
                if(fim > -1){
                    string = "<img src="+string.substring(string.indexOf("\""), fim) +"\" width=\"73\" height=\"68\">";
                }
            }else if(string.startsWith("$v:\"")){
                int fim = string.lastIndexOf("\"");
                int extenssao = string.substring(string.lastIndexOf("/")).lastIndexOf(".");
                System.out.println(string);
                if(fim > -1){
                    if(extenssao == -1){
                        System.out.println(string.substring(string.indexOf("\""), fim));
//                        String video = string.substring(string.indexOf("=")+1, string.lastIndexOf("\""));
//                        System.out.println(video);
                        
                        string = string.replace("watch?v=", "embed/");
                        System.out.println(string);
                        string = "<iframe width=\"320\" height=\"240\" src="+ string.substring(string.indexOf("\""), fim-1)+"\" frameborder=\"0\" ></iframe>";
                    }else{
                        System.out.println(string.substring(string.indexOf("\""), fim));
                        string = "<video width=\"320\" height=\"240\" controls>"
                                + "<source src="+string.substring(string.indexOf("\""), fim)+"\" >"
                                + "</video>";
//                        string = "<iframe width=\"320\" height=\"240\" src="+string.substring(string.indexOf("\""), fim)+"\" frameborder=\"0\"></iframe>";
//                        string = "<a target=\"_blank\" href="+string.substring(string.indexOf("\""), fim)+"\" ></a>";
                               
                    }
                }
            }else if(string.startsWith("$l:\"")){
                int fim = string.lastIndexOf("\"");
                if(fim > -1){
                    string = "<a href="+string.substring(string.indexOf("\""), fim) +"\">"+string+"</a>";
                }
            }else if(string.startsWith("@")){
                System.out.println("Usuario = " + string);
                try(DAOFactory daoFactory = new DAOFactory();){
                    GrupoDAO grupoDAO = daoFactory.getGrupoDAO();
                    Grupo grupo = grupoDAO.readNome(string.substring(1));
                    if(grupo != null)
                        string = "<a href=\""+caminho+"/grupo/read?id="+grupo.getId()+"\">"+string+"</a>";
                    else{
                        UsuarioDAO usuarioDAO = daoFactory.getUsuarioDAO();
                        Usuario usuario = usuarioDAO.buscarPorLogin(string.substring(1));
                        string = "<a href=\""+caminho+"/usuario/read?id="+usuario.getId()+"\">"+string+"</a>";
                    }
                        
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                }
            }
            
            
            textoComLink = textoComLink.concat(" "+string);
        }
        return textoComLink;
    }
    
    public String tratarBuscaDePost(String texto){
        System.out.println("Tema = " +texto );
        String termoBusca = "";
        String subConsulta = "";
        String[] tokens = texto.split(" ");
        List<String> usuarios = new ArrayList<>();
        List<String> temas = new ArrayList<>();
        
        for (String string : tokens) {
            if(string.startsWith(HASHTAG)){
                temas.add(string.substring(1));
            }else if(string.startsWith(USUARIO_GRUPO)){
                usuarios.add(string.substring(1));
            }
        }
        
        termoBusca = termoBusca.concat("SELECT * FROM post WHERE ");
        int i;
        if(!temas.isEmpty()){
            termoBusca = termoBusca.concat("id IN (SELECT id_post FROM hashtag WHERE ");
            i = 0;
            while(!temas.isEmpty()){
                  termoBusca = termoBusca.concat("tag = '"+temas.get(i)+"'");
                  temas.remove(i);
                  if(!temas.isEmpty()){
                      termoBusca = termoBusca.concat(" OR ");
                  }
            }
            termoBusca = termoBusca.concat(")");
            if(!usuarios.isEmpty()){
                termoBusca = termoBusca.concat(" AND id_autor IN (SELECT id FROM usuario WHERE ");
                subConsulta = subConsulta.concat(" UNION SELECT id_usuario FROM grupo_pertence WHERE id_grupo = "
                        + "(SELECT id FROM grupo WHERE ");
            }
        }else{
            if(!usuarios.isEmpty()){
                termoBusca = termoBusca.concat("id_autor IN (SELECT id FROM usuario WHERE ");
                subConsulta = subConsulta.concat(" UNION SELECT id_usuario FROM grupo_pertence WHERE id_grupo = "
                            + "(SELECT id FROM grupo WHERE ");
            }
        }
        i = 0;
        while(!usuarios.isEmpty()){
            termoBusca = termoBusca.concat(" login = '" +usuarios.get(i)+"'");
            subConsulta = subConsulta.concat(" nome = '" +usuarios.get(i)+"'");
            usuarios.remove(i);
            if(!usuarios.isEmpty()){
                termoBusca = termoBusca.concat(" OR ");
                subConsulta = subConsulta.concat(" OR ");
            }else{
                subConsulta = subConsulta.concat(")");
                termoBusca = termoBusca.concat(subConsulta);
                termoBusca = termoBusca.concat(")");
            }
        }
        
        return termoBusca;
    }
    
}
