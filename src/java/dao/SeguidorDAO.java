package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Seguidor;

public class SeguidorDAO extends DAO<Seguidor>{
    private final String createQuery = "INSERT INTO seguidor(id_seguido, id_seguidor) VALUES(?, ?);";
    private final String readQuery = "SELECT * FROM seguidor WHERE id_seguido = ? AND id_seguidor = ?";
    private final String deleteQuery = "DELETE FROM seguidor WHERE id_seguido = ? AND id_seguidor = ?";
    private final String allSeguidoresQuery = "SELECT * FROM seguidor WHERE id_seguido = ?";
    private final String allSeguidosQuery = "SELECT * FROM seguidor WHERE id_seguidor = ?";
    
    
    public SeguidorDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Seguidor seguidor) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            statement.setInt(1, seguidor.getId_seguido());
            statement.setInt(2, seguidor.getId_seguidor());
            
            statement.execute();
        }catch(SQLException e){
            System.err.println(e.getMessage());
        }
    }

    public Seguidor read(Integer id_seguido) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Seguidor read(Integer id_seguido, Integer id_seguidor) throws SQLException {
        Seguidor seguidor = new Seguidor();
        try(PreparedStatement statement = connection.prepareStatement(readQuery);){
            statement.setInt(1, id_seguido);
            statement.setInt(2, id_seguidor);
        
           try(ResultSet result = statement.executeQuery();){
               if(result.next()){
                   seguidor.setId_seguido(result.getInt("id_seguido"));
                   seguidor.setId_seguidor(result.getInt("id_seguidor"));
               }
           }catch(SQLException e){
               System.err.println(e.getMessage());
           }
           
        }catch(SQLException e){
            System.err.println(e.getMessage());
        }
        return seguidor;
    }

    @Override
    public void update(Seguidor t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void delete(Integer id_seguido, Integer id_seguidor) throws SQLException{
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setInt(1, id_seguido);
            statement.setInt(2, id_seguidor);
            
            if(statement.executeUpdate() < 1){
                throw new SQLException("Falha ao excluir: grupo não encontrado");
            }
        }
    }

    public void delete(Seguidor seguidor) throws SQLException{
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setInt(1, seguidor.getId_seguido());
            statement.setInt(2, seguidor.getId_seguidor());
            
            if(statement.executeUpdate() < 1){
                throw new SQLException("Falha ao excluir: grupo não encontrado");
            }
        }
    }
    
    @Override
    public List<Seguidor> all() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Seguidor> allSeguidores(Integer id_seguido) throws SQLException{
        List<Seguidor> seguidores = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allSeguidoresQuery);){
            statement.setInt(1, id_seguido);
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Seguidor seguidor = new Seguidor();
                seguidor.setId_seguido(id_seguido);
                seguidor.setId_seguidor(result.getInt("id_seguidor"));
                
                seguidores.add(seguidor);
            }
        }
        return seguidores;
    }
    
    public List<Seguidor> allSeguidos(Integer id_seguidor) throws SQLException{
        List<Seguidor> seguidores = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allSeguidosQuery);){
            statement.setInt(1, id_seguidor);
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Seguidor seguidor = new Seguidor();
                seguidor.setId_seguidor(id_seguidor);
                seguidor.setId_seguido(result.getInt("id_seguido"));
                
                seguidores.add(seguidor);
            }
        }
        return seguidores;
    }
    
    
}
