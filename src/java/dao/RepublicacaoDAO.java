package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Post;
import model.Republicacao;

public class RepublicacaoDAO extends DAO<Republicacao>{
    private final String createQuery = "INSERT INTO republicacao(id_post, id_usuario_republicando) VALUES (?,?);";
    private final String readQuery = "SELECT * FROM republicacao WHERE id_post = ? AND id_usuario_republicando = ?";
    private final String deleteQuery = "DELETE FROM republicacao WHERE id_post = ? AND id_usuario_republicando = ?;";
    private final String allRepublicacoesUsuario = "SELECT * FROM republicacao WHERE id_usuario_republicando = ?";
    private final String allQuery = "SELECT * FROM republicacao";
    
    public RepublicacaoDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Republicacao republicacao) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            statement.setInt(1, republicacao.getId_post());
            statement.setInt(2, republicacao.getId_usuario_republicando());
            
            statement.executeUpdate();
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
    }

    @Override
    public Republicacao read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Republicacao read(Integer id_post, Integer id_usuario_republicando) throws SQLException {
        Republicacao republicacao =  new Republicacao();
        try(PreparedStatement statement = connection.prepareStatement(readQuery);){
            
            statement.setInt(1, id_post);
            statement.setInt(2, id_usuario_republicando);
            
            try (ResultSet result = statement.executeQuery();){

                if (result.next()) {
                    
                    republicacao.setId_post(result.getInt("id_post"));
                    republicacao.setId_usuario_republicando(result.getInt("id_usuario_republicando"));
                    
                } else {
                    return null;
                }
            } 
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
        return republicacao;
    }
    
    @Override
    public void update(Republicacao t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void delete(Republicacao republicacao) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setInt(1, republicacao.getId_post()); 
            statement.setInt(2, republicacao.getId_usuario_republicando());
            
            if(statement.executeUpdate() < 1){
                throw new SQLException("Falha ao excluir: republicacao não encontrado");
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }  
    }

    @Override
    public List<Republicacao> all() throws SQLException {
        List<Republicacao> republicacoes = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allQuery);){
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Republicacao republicacao = new Republicacao();
                
                republicacao.setId_post(result.getInt("id_post"));
                republicacao.setId_usuario_republicando(result.getInt("id_usuario_republicando"));
                republicacao.setData_republicacao(result.getTimestamp("data_republicacao"));
                
                republicacoes.add(republicacao);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return republicacoes;
    }
    
    public List<Republicacao> allRepublicacoesUsuario(Integer id_usuario_republicando){
        List<Republicacao> republicacoes = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allRepublicacoesUsuario);){
            statement.setInt(1, id_usuario_republicando);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Republicacao republicacao = new Republicacao();
                
                republicacao.setId_post(result.getInt("id_post"));
                republicacao.setId_usuario_republicando(result.getInt("id_usuario_republicando"));
                republicacao.setData_republicacao(result.getTimestamp("data_republicacao"));
                
                republicacoes.add(republicacao);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return republicacoes;
        
    }
    
}
