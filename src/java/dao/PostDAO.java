package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Post;

public class PostDAO extends DAO<Post>{
    
    private final String createQuery = "INSERT INTO post(texto, id_autor, titulo) VALUES(?, ?, ?) RETURNING id;";
    private final String readQuery = "SELECT * FROM post WHERE id = ?;";
    private final String updateQuery = "UPDATE post SET texto = ?, titulo = ? WHERE id = ?;";
    private final String deleteQuery = "DELETE FROM post WHERE id = ?;";
    private final String allQuery = "SELECT * FROM post ORDER BY data_postagem DESC;";
//    private final String allAutorQuery = "SELECT * FROM post WHERE id_autor = ? ORDER BY data_postagem DESC";
    private final String allAutorQuery = "SELECT id, titulo, texto, id_autor, data_postagem FROM post WHERE id_autor = ? "
            + "EXCEPT (SELECT p.id, titulo, texto, id_autor, data_postagem FROM post p, comentario c WHERE p.id = c.id AND p.id_autor = ?) "
            + "ORDER BY data_postagem DESC";
//    private final String allPostQuery = "SELECT * FROM (post p LEFT JOIN republicacao r ON p.id = r.id_post) "
//            + "WHERE r.id_usuario_republicando = ? OR p.id_autor = ?;";
    private final String allPostQuery = "SELECT id, titulo, texto, id_autor FROM (post p LEFT JOIN republicacao r ON p.id = r.id_post) "
            + "WHERE r.id_usuario_republicando = ? OR p.id_autor = ? "
            + "EXCEPT SELECT p.id, titulo, texto, id_autor FROM post p, comentario c "
            +"WHERE p.id = c.id AND p.id_autor = ?";
    private final String readPostRecente = "SELECT * FROM post WHERE id_autor = ?"
            + " ORDER BY data_postagem DESC LIMIT 1;";
    
    private final String busca = "?";
    
    public PostDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(Post post){
                throw new UnsupportedOperationException("Not supported yet."); 
    }
    public int createRetornaID(Post post){
        int id = -1;
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            
            statement.setString(1, post.getTexto());
            statement.setLong(2, post.getId_autor());
            statement.setString(3, post.getTitulo());
            
            ResultSet result = statement.executeQuery();
            if(result.next()){
                id = result.getInt("id");
            }
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return id;
    }
    
    @Override
    public Post read(Integer id){
        Post post = new Post();
        
        try(PreparedStatement statement = connection.prepareStatement(readQuery);){
            statement.setLong(1, id);
            
           // statement.executeUpdate();
            try (ResultSet result = statement.executeQuery();) {
                if(result.next()){
                    post.setId(result.getInt("id"));
                    post.setTexto(result.getString("texto"));
                    post.setId_autor(result.getInt("id_autor"));
                    post.setTitulo(result.getString("titulo"));
                    post.setData_postagem(result.getTimestamp("data_postagem"));
                    post.setData_atualizacao_postagem(result.getTimestamp("data_atualizacao_postagem"));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return post;  
    }
    
    @Override
    public void update(Post post){
     try(PreparedStatement statement = connection.prepareStatement(updateQuery);){
            statement.setString(1, post.getTexto());
            statement.setString(2, post.getTitulo());
            statement.setLong(3, post.getId());
            
            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao editar: post não encontrado.");
            }
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public void delete(Integer id){
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setLong(1, id);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao excluir: post não encontrado.");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public List<Post> all(){
        List<Post> posts = new ArrayList<Post>();
        
        try(PreparedStatement statement = connection.prepareStatement(allQuery);){
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Post post = new Post();
                post.setId(result.getInt("id"));
                post.setTexto(result.getString("texto"));
                post.setId_autor(result.getInt("id_autor"));
                post.setTitulo(result.getString("titulo"));
                post.setData_postagem(result.getTimestamp("data_postagem"));
                post.setData_atualizacao_postagem(result.getTimestamp("data_atualizacao_postagem"));

                
                posts.add(post);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return posts;
    }

    public List<Post> allPostAutor(Integer id_autor){
        List<Post> posts = new ArrayList<Post>();
        
        try(PreparedStatement statement = connection.prepareStatement(allAutorQuery);){
            statement.setInt(1, id_autor);
            statement.setInt(2, id_autor);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Post post = new Post();
                post.setId(result.getInt("id"));
                post.setTexto(result.getString("texto"));
                post.setTitulo(result.getString("titulo"));
                post.setData_postagem(result.getTimestamp("data_postagem"));
                post.setId_autor(id_autor);
                
                posts.add(post);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return posts;
    }
    
    public List<Post> allPost(Integer id_autor){
        List<Post> posts = new ArrayList<Post>();
        
        try(PreparedStatement statement = connection.prepareStatement(allPostQuery);){
            statement.setInt(1, id_autor);
            statement.setInt(2, id_autor);
            statement.setInt(3, id_autor);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Post post = new Post();
                post.setId(result.getInt("id"));
                post.setTexto(result.getString("texto"));
                post.setTitulo(result.getString("titulo"));
                post.setId_autor(result.getInt("id_autor"));
                
                posts.add(post);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return posts;
    }
    
    public Post readPostRecente(Integer id_autor){
        Post post = new Post();
        
        try(PreparedStatement statement = connection.prepareStatement(readPostRecente);){
            statement.setLong(1, id_autor);
            
           // statement.executeUpdate();
            try (ResultSet result = statement.executeQuery();) {
                if(result.next()){
                    post.setId(result.getInt("id"));
                    post.setTexto(result.getString("texto"));
                    post.setId_autor(result.getInt("id_autor"));
                    post.setTitulo(result.getString("titulo"));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return post;  
    }
    
    public List<Post> buscarPost(String query) throws SQLException{
        List<Post> posts = new ArrayList<Post>();
        
        try(Statement statement = connection.createStatement();){

            ResultSet result = statement.executeQuery(query);
            
            while(result.next()){
                Post post = new Post();
                post.setId(result.getInt("id"));
                post.setTexto(result.getString("texto"));
                post.setTitulo(result.getString("titulo"));
                post.setId_autor(result.getInt("id_autor"));
                
                posts.add(post);
            }
        }   
        return posts;
    }
    
}