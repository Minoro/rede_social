/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dao;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Usuario;

public class UsuarioDAO extends DAO<Usuario> {
    private static final String createQuery = "INSERT INTO usuario(login, senha, nome, data_nascimento, descricao, foto) VALUES(?, md5(?), ?, ?, ?, ?);";
    private static final String readQuery = "SELECT * FROM usuario WHERE id = ?;";
    private static final String updateQuery = "UPDATE usuario SET login = ?, nome = ?, data_nascimento = ?, descricao = ?, foto = ? WHERE id = ?;";
    private static final String updateWithPasswordQuery = "UPDATE usuario SET login = ?, nome = ?, data_nascimento = ?, descricao = ?, foto = ?, senha = md5(?) WHERE id = ?;";
    private static final String deleteQuery = "DELETE FROM usuario WHERE id = ?;";
    private static final String allQuery = "SELECT * FROM usuario;";
    private static final String allSeguidos = "SELECT * FROM usuario "
            + "WHERE id IN (SELECT id_seguido FROM seguidor WHERE id_seguidor= ?);";
    private static final String allNaoSeguidos = "SELECT * FROM usuario "
            + "WHERE id != ? AND id NOT IN(SELECT id_seguido FROM seguidor WHERE id_seguidor = ?)";
    private static final String authenticateQuery = "SELECT id, senha, nome, data_nascimento, descricao, foto FROM usuario WHERE login = ?;";
    private static final String buscaQuery = "SELECT * FROM usuario WHERE nome LIKE ? OR login LIKE ? OR descricao LIKE ?";
    private static final String buscaLogin = "SELECT * FROM usuario WHERE login = ?";
    
    public UsuarioDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Usuario usuario) throws SQLException {
        
        try (PreparedStatement statement = connection.prepareStatement(createQuery);) {
            statement.setString(1, usuario.getLogin());
            statement.setString(2, usuario.getSenha());
            statement.setString(3, usuario.getNome());
            statement.setTimestamp(4, usuario.getData_nascimento());
            statement.setString(5, usuario.getDescricao());
            statement.setString(6, usuario.getFoto());
            
            statement.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }
    }

    @Override
    public Usuario read(Integer id) throws SQLException {
        Usuario usuario = new Usuario();

        try (PreparedStatement statement = connection.prepareStatement(readQuery);) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    usuario.setId(result.getInt("id"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setData_nascimento(result.getTimestamp("data_nascimento"));
                    usuario.setDescricao(result.getString("descricao"));
                    usuario.setFoto(result.getString("foto"));
                    
                } else {
                    throw new SQLException("Falha ao visualizar: usuário não encontrado.");
                }
            }
        }

        return usuario;
    }

    @Override
    public void update(Usuario usuario) throws SQLException {
        String query;

        if (usuario.getSenha() == null) {
            query = updateQuery;
        } else {
            query = updateWithPasswordQuery;
        }

        try (PreparedStatement statement = connection.prepareStatement(query);) {
            statement.setString(1, usuario.getLogin());
            statement.setString(2, usuario.getNome());
            statement.setTimestamp(3, usuario.getData_nascimento());
            statement.setString(4, usuario.getDescricao());
            statement.setString(5, usuario.getFoto());

            if (usuario.getSenha() != null) {
                statement.setString(6, usuario.getSenha());
                statement.setInt(7, usuario.getId());
            } else {
                statement.setInt(6, usuario.getId());
            }

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao editar: usuário não encontrado.");
            }
        }
    }

    @Override
    public void delete(Integer id) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(deleteQuery);) {
            statement.setInt(1, id);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao excluir: usuário não encontrado.");
            }
        }
    }

    @Override
    public List<Usuario> all() throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();
        
        try (PreparedStatement statement = connection.prepareStatement(allQuery);
            ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                
                usuario.setNome(result.getString("nome"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDescricao(result.getString("descricao"));
                usuario.setFoto(result.getString("foto"));
                usuario.setData_nascimento(result.getTimestamp("data_nascimento"));

                usuarioList.add(usuario);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }
    
    public List<Usuario> allSeguidos(Integer id) throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();
        
        try (PreparedStatement statement = connection.prepareStatement(allSeguidos);){
            
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                
                usuario.setNome(result.getString("nome"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDescricao(result.getString("descricao"));
                usuario.setFoto(result.getString("foto"));
                usuario.setData_nascimento(result.getTimestamp("data_nascimento"));

                usuarioList.add(usuario);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }
    
    public List<Usuario> allNaoSeguidos(Integer id) throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();
        
        try (PreparedStatement statement = connection.prepareStatement(allNaoSeguidos);){
            
            statement.setInt(1, id);
            statement.setInt(2, id);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                
                usuario.setNome(result.getString("nome"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDescricao(result.getString("descricao"));
                usuario.setFoto(result.getString("foto"));
                usuario.setData_nascimento(result.getTimestamp("data_nascimento"));

                usuarioList.add(usuario);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }
    
    
    
    public void authenticate(Usuario usuario) throws SQLException, SecurityException {
        try (PreparedStatement statement = connection.prepareStatement(authenticateQuery);) {
            statement.setString(1, usuario.getLogin());
            
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    MessageDigest md5;

                    String senhaForm;
                    String senhaUsuario;

                    try {
                        md5 = MessageDigest.getInstance("MD5");
                        md5.update(usuario.getSenha().getBytes());

                        senhaForm = new BigInteger(1, md5.digest()).toString(16);
                        senhaUsuario = result.getString("senha");

                        if (!senhaForm.equals(senhaUsuario)) {
                            throw new SecurityException("Senha incorreta.");
                        }
                    } catch (NoSuchAlgorithmException ex) {
                        System.err.println(ex.getMessage());
                    }

                    usuario.setId(result.getInt("id"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setDescricao(result.getString("descricao"));
                    usuario.setData_nascimento(result.getTimestamp("data_nascimento"));
                    usuario.setFoto(result.getString("foto"));
                } else {
                    throw new SecurityException("Login incorreto.");
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }
    }
    
    
    public List<Usuario> buscar(String s) throws SQLException {
        List<Usuario> usuarioList = new ArrayList<>();
        
        try (PreparedStatement statement = connection.prepareStatement(buscaQuery);){
            
            statement.setString(1, s);
            statement.setString(2, s);
            statement.setString(3, s);
                    
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(result.getInt("id"));
                usuario.setLogin(result.getString("login"));
                
                usuario.setNome(result.getString("nome"));
                usuario.setSenha(result.getString("senha"));
                usuario.setDescricao(result.getString("descricao"));
                usuario.setFoto(result.getString("foto"));
                usuario.setData_nascimento(result.getTimestamp("data_nascimento"));

                usuarioList.add(usuario);
            }
        } catch (SQLException ex) {
            throw ex;
        }

        return usuarioList;
    }
    
    public Usuario buscarPorLogin(String login) throws SQLException {
        Usuario usuario = new Usuario();

        try (PreparedStatement statement = connection.prepareStatement(buscaLogin);) {
            statement.setString(1, login);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    usuario.setId(result.getInt("id"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setData_nascimento(result.getTimestamp("data_nascimento"));
                    usuario.setDescricao(result.getString("descricao"));
                    usuario.setFoto(result.getString("foto"));
                    
                } else {
                    throw new SQLException("Falha ao visualizar: usuário não encontrado.");
                }
            }
        }

        return usuario;
    }
    
    
    
}
