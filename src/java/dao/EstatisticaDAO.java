package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.PostEstatistica;
import model.Usuario;
import model.UsuarioEstatistica;
import model.UsuarioSimilar;

public class EstatisticaDAO extends DAO<UsuarioEstatistica>{

    private final String top20Query = "select *, calcular_influencia(id, ?, ?) as influencia from usuario order by influencia desc limit 20;";
    private final String top3Query = "select *, calcular_influencia(id, ?, ?) as influencia from usuario order by influencia desc limit 3;";
    private final String top20PostQuery = "select *, calcular_impacto(id, ?, ?) as impacto from post order by impacto desc limit 20;";
    private final String usuarioSimilaresQuery = "select *, calcular_similaridade(u.id,?) as similaridade from usuario u order by similaridade desc limit 10;";
    private final String zonaInfluenciaQuery = "select calcular_zona_influencia(?) as zona_influencia;";
    private final String flutuanciaQuery = "select calcular_flutuancia(?, ?, ?) as flutuancia;";
    
    public EstatisticaDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(UsuarioEstatistica t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UsuarioEstatistica read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(UsuarioEstatistica t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<UsuarioEstatistica> all() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<UsuarioEstatistica> listTop20(String data_inicio, String data_fim) throws SQLException{
        List<UsuarioEstatistica> usuarios = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(top20Query);){
            statement.setString(1, data_inicio);
            statement.setString(2, data_fim);
            
            try(ResultSet result = statement.executeQuery();){
                while(result.next()){
                    UsuarioEstatistica usuario = new UsuarioEstatistica();
                    usuario.setId(result.getInt("id"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setDescricao(result.getString("descricao"));
                    usuario.setData_nascimento(result.getTimestamp("data_nascimento"));
                    usuario.setInfluencia(result.getInt("influencia"));
                    
                    usuarios.add(usuario);
                }
            }
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return usuarios;
    }
    public List<UsuarioEstatistica> listTop3(String data_inicio, String data_fim) throws SQLException{
        List<UsuarioEstatistica> usuarios = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(top3Query);){
            statement.setString(1, data_inicio);
            statement.setString(2, data_fim);
            
            try(ResultSet result = statement.executeQuery();){
                while(result.next()){
                    UsuarioEstatistica usuario = new UsuarioEstatistica();
                    usuario.setId(result.getInt("id"));
                    usuario.setNome(result.getString("nome"));
                    usuario.setLogin(result.getString("login"));
                    usuario.setDescricao(result.getString("descricao"));
                    usuario.setData_nascimento(result.getTimestamp("data_nascimento"));
                    usuario.setInfluencia(result.getInt("influencia"));
                    
                    usuarios.add(usuario);
                }
            }
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return usuarios;
    }
    
    public List<PostEstatistica> listTop20Post(String data_inicio, String data_fim) throws SQLException{
        List<PostEstatistica> posts = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(top20PostQuery);){
            statement.setString(1, data_inicio);
            statement.setString(2, data_fim);
            
            try(ResultSet result = statement.executeQuery();){
                while(result.next()){
                    PostEstatistica post = new PostEstatistica();
                    post.setId(result.getInt("id"));
                    post.setTitulo(result.getString("titulo"));
                    post.setTexto(result.getString("texto"));
                    post.setId_autor(result.getInt("id_autor"));
                    post.setData_postagem(result.getTimestamp("data_postagem"));
                    post.setImpacto(result.getInt("impacto"));
                    
                    posts.add(post);
                }
            }
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return posts;
    }
    
    public List<UsuarioSimilar> listUsuariosSimilares(Integer id) throws SQLException{
        List<UsuarioSimilar> usuarios = new ArrayList<>();
    
        try(PreparedStatement statement = connection.prepareStatement(usuarioSimilaresQuery);){
             statement.setInt(1, id);
             
             try(ResultSet result = statement.executeQuery();){
                 while(result.next()){
                     UsuarioSimilar usuario = new UsuarioSimilar();
                     System.out.println(result.getString("login"));
                     usuario.setId(result.getInt("id"));
                     usuario.setLogin(result.getString("login"));
                     usuario.setNome(result.getString("nome"));
                     usuario.setDescricao(result.getString("descricao"));
                     usuario.setSimilaridade(result.getFloat("similaridade"));
                     
                     usuarios.add(usuario); 
                 }
             }
         }catch(SQLException e){
             throw new RuntimeException(e);
         }
        return usuarios;
    }
    
    public List<Float> calcularFlutuancia(int id, String data_inicio, String data_fim) throws SQLException{
        List<Float> flutuancia = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(flutuanciaQuery);){
            statement.setInt(1, id);
            statement.setString(2, data_inicio);
            statement.setString(3, data_fim);

            try(ResultSet result = statement.executeQuery();){
                while(result.next()){
                    Float f = result.getFloat("flutuancia");
                    
                    flutuancia.add(f);
                }
            }
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return flutuancia;
    }
    
    public int zonaInfluencia(Integer id){
        int tamanhoZonaInfluencia = 0;
        
        try(PreparedStatement statement = connection.prepareStatement(zonaInfluenciaQuery);){
             statement.setInt(1, id);
             
             try(ResultSet result = statement.executeQuery();){
                 if(result.next()){
                     tamanhoZonaInfluencia = result.getInt("zona_influencia");
                 }
             }
         }catch(SQLException e){
             throw new RuntimeException(e);
         }
        return tamanhoZonaInfluencia;
    }
    
}
