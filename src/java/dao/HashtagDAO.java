package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Hashtag;
import model.Post;

public class HashtagDAO extends DAO<Hashtag>{

    private final String createQuery = "INSERT INTO hashtag(id_post, tag) VALUES(?, ?);";
    private final String deleteQuery = "DELETE FROM hashtag WHERE id_post = ?;";
    private final String allQuery = "SELECT * FROM hashtag WHERE tag = ?;";
    private final String postHashtag = "SELECT * FROM post p, hashtag h WHERE p.id = h.id_post "
            + "AND tag = ?;";
    
    
    public HashtagDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Hashtag hashtag) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            statement.setLong(1, hashtag.getId_post());
            statement.setString(2, hashtag.getHashtag());
            
            statement.executeUpdate();
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
    }

    @Override
    public Hashtag read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Hashtag t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id_post) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setInt(1, id_post);
            
            if(statement.executeUpdate() < 1){
                throw new SQLException("Falha ao excluir: hashtag não encontrado");
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
    }

    @Override
    public List<Hashtag> all() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<Hashtag> all(String tag) throws SQLException {
        List<Hashtag> postsHashtag = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allQuery);){
            statement.setString(1, tag);
            
            ResultSet result = statement.executeQuery();
            
            if(result.next()){
                Hashtag hashtag = new Hashtag();
                
                hashtag.setId_post(result.getInt("id_post"));
                hashtag.setHashtag(result.getString("tag"));
                
                postsHashtag.add(hashtag);
            }
            
        }
        return postsHashtag;
    }
    
    
    public List<Post> postHashtag(String tag) throws SQLException{
        List<Post> postsHashtag = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(postHashtag);){
            statement.setString(1, tag);
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Post post = new Post();
                
                post.setId(result.getInt("id"));
                post.setId_autor(result.getInt("id_autor"));
                post.setTexto(result.getString("texto"));
                post.setTitulo(result.getString("titulo"));
                
                postsHashtag.add(post);
            }
            
        }
        return postsHashtag;
        
    }
}
