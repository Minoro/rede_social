package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Post;
import model.PostComUsuario;

public class PostComUsuarioDAO extends DAO<PostComUsuario>{

    private final String readPostsAutorQueary = "SELECT * FROM post p, usuario u WHERE p.id_autor = u.id AND p.id_autor = ?";
    private final String allComentarios = "SELECT p.id, p.titulo, p.texto, p.id_autor, u.login FROM post p, comentario c, usuario u WHERE p.id = c.id AND p.id_autor = u.id AND c.id_post = ?;";
    
    private final String allPostQuery = "SELECT p.id, titulo, texto, id_autor, login, data_postagem FROM ((post p LEFT JOIN republicacao r ON p.id = r.id_post) "
                                        +"LEFT JOIN usuario u ON id_autor = u.id) " 
                                        +"WHERE (r.id_usuario_republicando = ? OR u.id = ?) "
                                        +"EXCEPT (SELECT p.id, titulo, texto, id_autor, login, data_postagem FROM post p, comentario c, usuario u "
                                        +"WHERE p.id = c.id AND p.id_autor = ? ) ORDER BY data_postagem DESC";
    
    
    
    public PostComUsuarioDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(PostComUsuario t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PostComUsuario read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(PostComUsuario t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PostComUsuario> all() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<PostComUsuario> allPostAutor(Integer id_autor){
        List<PostComUsuario> posts = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(readPostsAutorQueary);){
            statement.setInt(1, id_autor);
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                PostComUsuario post = new PostComUsuario();
                post.setId(result.getInt("id"));
                post.setTexto(result.getString("texto"));
                post.setTitulo(result.getString("titulo"));
                post.setNome(result.getString("login"));
                post.setId_autor(id_autor);
                
                posts.add(post);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return posts;
        
    }
    
    public List<PostComUsuario> allComentarios(Integer id) throws SQLException{
        List<PostComUsuario> comentarios = new ArrayList<>();
        try(PreparedStatement statement = connection.prepareStatement(allComentarios);){
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                PostComUsuario comentario = new PostComUsuario();
                comentario.setId(result.getInt("id"));
                comentario.setTexto(result.getString("texto"));
                comentario.setId_autor(result.getInt("id_autor"));
                comentario.setTitulo(result.getString("titulo"));
                comentario.setNome(result.getString("login"));
                
                comentarios.add(comentario);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return comentarios;
        
    }
    
    public List<PostComUsuario> allPost(Integer id_autor){
        List<PostComUsuario> posts = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allPostQuery);){
            statement.setInt(1, id_autor);
            statement.setInt(2, id_autor);
            statement.setInt(3, id_autor);
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                PostComUsuario post = new PostComUsuario();
                post.setId(result.getInt("id"));
                post.setTexto(result.getString("texto"));
                post.setTitulo(result.getString("titulo"));
                post.setId_autor(result.getInt("id_autor"));
                post.setNome(result.getString("login"));
                
                posts.add(post);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return posts;
    }
    
}
