package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Curtir;

public class CurtirDAO extends DAO<Curtir>{

    private final String createQuery = "INSERT INTO curtir(id_post, id_usuario, tipo) VALUES(?, ?, ?);";
    private final String readQuery = "SELECT * FROM curtir WHERE id_post = ? AND id_usuario = ?;";
    private final String updateQuery = "UPDATE curtir SET tipo = ?, data_curtir = current_timestamp WHERE id_post = ? AND id_usuario = ?;";
    private final String deleteQuery = "DELETE FROM curtir WHERE id_post = ? AND id_usuario = ?;";
    private final String countCurtirQuery = "SELECT COUNT(*) FROM curtir WHERE id_post = ? AND tipo = ?;";
    private final String allQuery = "SELECT * FROM curtir";
    
    public CurtirDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Curtir curtir) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            
            statement.setInt(1, curtir.getId_post());
            statement.setInt(2, curtir.getId_usuario());
            statement.setBoolean(3, curtir.getTipo());
            
            statement.executeUpdate();
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Curtir read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Curtir read(Integer id_post, Integer id_usuario) throws SQLException{
        Curtir curtir =  new Curtir();
        try(PreparedStatement statement = connection.prepareStatement(readQuery);){
            
            statement.setInt(1, id_post);
            statement.setInt(2, id_usuario);
            
            try (ResultSet result = statement.executeQuery();){

                if (result.next()) {
                    
                    curtir.setId_post(result.getInt("id_post"));
                    curtir.setId_usuario(result.getInt("id_usuario"));
                    curtir.setTipo(result.getBoolean("tipo"));
                    
                } else {
                    return null;
//                    throw new SQLException("Falha ao visualizar: curtir não encontrado.");
                }
            } 
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return curtir;
    }

    @Override
    public void update(Curtir curtir) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(updateQuery);){
            statement.setBoolean(1, curtir.getTipo());
            statement.setLong(2, curtir.getId_post());
            statement.setLong(3, curtir.getId_usuario());
            
            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao editar: post não encontrado.");
            }
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void delete(Integer id_post, Integer id_usuario){
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setInt(1, id_post);
            statement.setInt(2, id_usuario);

            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao excluir: curtir não encontrado.");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    

    @Override
    public List<Curtir> all() throws SQLException {
        List<Curtir> curtidas = new ArrayList<Curtir>();
        
        try(PreparedStatement statement = connection.prepareStatement(allQuery);){
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Curtir curtir = new Curtir();
                curtir.setId_post(result.getInt("id_post"));
                curtir.setId_usuario(result.getInt("id_usuario"));
                curtir.setTipo(result.getBoolean("tipo"));
                curtir.setData_curtir(result.getTimestamp("data_curtir"));
                
                curtidas.add(curtir);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return curtidas;
    }
    
    public int countCurtir(Integer id_post){
       int curtir = 0;
       try(PreparedStatement statement = connection.prepareStatement(countCurtirQuery);){
            statement.setLong(1, id_post);
            statement.setBoolean(2, true);
            
            ResultSet result = statement.executeQuery();
            if(result.next()){
                curtir = result.getInt(1);
            }
                    
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } 
        return curtir;
    }
    
    public int naoCountCurtir(Integer id_post){
       int curtir = 0;
       try(PreparedStatement statement = connection.prepareStatement(countCurtirQuery);){
            statement.setLong(1, id_post);
            statement.setBoolean(2, false);
            
            ResultSet result = statement.executeQuery();
            if(result.next()){
                curtir = result.getInt(1);
            }
                    
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } 
        return curtir;
    }
    
    
    
}
