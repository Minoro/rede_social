package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.GrupoPertence;
import model.Usuario;

public class GrupoPertenceDAO extends DAO<GrupoPertence> {
    private final String createQuery = "INSERT INTO grupo_pertence(id_grupo, id_usuario) VALUES(?, ?);";
    private final String readQuery = "SELECT * FROM grupo_pertence WHERE id_grupo = ? AND id_usuario = ?;";
    private final String deleteQuery = "DELETE FROM grupo_pertence WHERE id_grupo = ? AND id_usuario = ?;";
    private final String allQuery = "SELECT * FROM grupo_pertence;";
    private final String allGruposPertenceQuery = "SELECT * FROM grupo_pertence WHERE id_usuario = ?;";
    private final String allMembrosGrupo = "SELECT u.nome, u.login, u.id, u.data_nascimento, u.foto, u.senha FROM grupo_pertence gp, usuario u"
                                                +" WHERE gp.id_grupo = ? AND gp.id_usuario = u.id;";
    private final String allMembrosForaDoGrupo = "SELECT * FROM usuario u WHERE NOT EXISTS" +
                                                    "(SELECT us.id FROM grupo_pertence gp, usuario us " +
                                                    "WHERE gp.id_grupo = ? AND gp.id_usuario = u.id);";
    
    public GrupoPertenceDAO(Connection connection) {
        super(connection);
    }
    
    @Override
    public void create(GrupoPertence grupoPertence){
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            statement.setInt(1, grupoPertence.getId_grupo());
            statement.setInt(2, grupoPertence.getId_usuario());
            
            statement.executeUpdate();
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public GrupoPertence read(Integer id){
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public GrupoPertence read(Integer id_grupo, Integer id_usuario){
        GrupoPertence grupoPertence = new GrupoPertence();
        
        try(PreparedStatement statement = connection.prepareStatement(readQuery);){
            statement.setInt(1, id_grupo);
            statement.setInt(2, id_usuario);
            
            try(ResultSet result = statement.executeQuery()){
                if(result.next()){
                    grupoPertence.setId_grupo(result.getInt("id_grupo"));
                    grupoPertence.setId_usuario(result.getInt("id_usuario"));   
                }
            }catch(SQLException e){
                throw new RuntimeException(e);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return grupoPertence;
    }

    @Override
    public void update(GrupoPertence t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void delete(Integer id_grupo, Integer id_usuario){
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setInt(1, id_grupo); 
            statement.setInt(2, id_usuario);
            
            if(statement.executeUpdate() < 1){
                throw new SQLException("Falha ao excluir: grupo não encontrado");
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }   
    }

    @Override
    public List<GrupoPertence> all() throws SQLException {
        List<GrupoPertence> gruposPertence = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allQuery);){
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                GrupoPertence grupoPertence = new GrupoPertence();
                
                grupoPertence.setId_grupo(result.getInt("id_grupo"));
                grupoPertence.setId_usuario(result.getInt("id_usuario"));
                
                gruposPertence.add(grupoPertence);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return gruposPertence;
    }
    
    public List<GrupoPertence> allGrupoPertence(Integer id) throws SQLException {
        List<GrupoPertence> gruposPertence = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allGruposPertenceQuery);){
            statement.setInt(1, id);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                GrupoPertence grupoPertence = new GrupoPertence();
                
                grupoPertence.setId_grupo(result.getInt("id_grupo"));
                grupoPertence.setId_usuario(result.getInt("id_usuario"));
                
                gruposPertence.add(grupoPertence);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return gruposPertence;
    }
            
    public List<Usuario> allMembrosGrupo(Integer id_grupo) throws SQLException{
        List<Usuario> membrosGrupo = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allMembrosGrupo);){
            statement.setInt(1, id_grupo);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Usuario usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                usuario.setLogin(result.getString("login"));
                
                membrosGrupo.add(usuario);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return membrosGrupo;
    }
    
    public List<Usuario> allMembrosForaDoGrupo(Integer id_grupo) throws SQLException{
        List<Usuario> membrosForaDoGrupo = new ArrayList<>();
        
        try(PreparedStatement statement = connection.prepareStatement(allMembrosForaDoGrupo);){
            statement.setInt(1, id_grupo);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Usuario usuario = new Usuario();
                
                usuario.setId(result.getInt("id"));
                usuario.setNome(result.getString("nome"));
                usuario.setLogin(result.getString("login"));
                
                membrosForaDoGrupo.add(usuario);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return membrosForaDoGrupo;
    }
    
    
}
