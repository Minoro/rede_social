package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Comentario;
import model.ComentarioParaJSON;
import model.Post;

public class ComentarioDAO extends DAO<Comentario>{

    private final String createQuery = "INSERT INTO comentario(id, id_post) VALUES (?, ?);";
    private final String readQuery = "SELECT * FROM comentario c, post p WHERE c.id_post = p.id AND c.id = ?;";
//    private final String deleteQuery = "DELETE FROM comentario WHERE id = ?;";
//    private final String allComentarios = "SELECT p.id, p.titulo, p.texto, p.id_autor FROM comentario c, post p WHERE c.id_post = p.id AND p.id = ?";
    private final String allComentarios = "SELECT p.id, p.titulo, p.texto, p.id_autor, p.data_postagem FROM post p, comentario c WHERE p.id = c.id AND c.id_post = ?;";
    private final String allQuery = "SELECT c.id as id_comentario, p.id, p.titulo, p.texto, p.id_autor, p.data_postagem FROM post p, comentario c WHERE p.id = c.id";
    
    public ComentarioDAO(Connection connection) {
        super(connection);
    }

    @Override
    public void create(Comentario comentario) throws SQLException {
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            statement.setInt(1, comentario.getId());
            statement.setInt(2, comentario.getId_post());
            
            statement.executeUpdate();
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public Comentario read(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Post readComentario(Integer id) throws SQLException{
        Post post = new Post();
        
        try(PreparedStatement statement = connection.prepareStatement(readQuery);){
            statement.setLong(1, id);
            
           // statement.executeUpdate();
            try (ResultSet result = statement.executeQuery();) {
                if(result.next()){
                    post.setId(result.getInt("id"));
                    post.setTexto(result.getString("texto"));
                    post.setId_autor(result.getInt("id_autor"));
                    post.setTitulo(result.getString("titulo"));
                    post.setData_postagem(result.getTimestamp("data_postagem"));
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return post;
    
    }

    @Override
    public void update(Comentario t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Integer id) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Comentario> all() throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<ComentarioParaJSON> allComentario() throws SQLException{
        List<ComentarioParaJSON> comentarios = new ArrayList<>();
        try(PreparedStatement statement = connection.prepareStatement(allQuery);){
            
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                ComentarioParaJSON comentario = new ComentarioParaJSON();
                comentario.setId(result.getInt("id"));
                comentario.setId_comentario(result.getInt("id_comentario"));
                comentario.setTexto(result.getString("texto"));
                comentario.setId_autor(result.getInt("id_autor"));
                comentario.setTitulo(result.getString("titulo"));
                comentario.setData_postagem(result.getTimestamp("data_postagem"));
                
                comentarios.add(comentario);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return comentarios;
    }
    
    public List<Post> allComentarios(Integer id) throws SQLException{
        List<Post> comentarios = new ArrayList<>();
        try(PreparedStatement statement = connection.prepareStatement(allComentarios);){
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Post comentario = new Post();
                comentario.setId(result.getInt("id"));
                comentario.setTexto(result.getString("texto"));
                comentario.setId_autor(result.getInt("id_autor"));
                comentario.setTitulo(result.getString("titulo"));
                comentario.setData_postagem(result.getTimestamp("data_postagem"));
                
                comentarios.add(comentario);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return comentarios;
        
    }
    
}
