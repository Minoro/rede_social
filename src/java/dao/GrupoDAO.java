package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Grupo;

public class GrupoDAO extends DAO<Grupo>{
    private final String createQuery = "INSERT INTO grupo(nome, descricao, id_proprietario) VALUES(?, ?, ?);";
    private final String readQuery = "SELECT * FROM grupo WHERE id = ?;";
    private final String readNomeQuery = "SELECT * FROM grupo WHERE nome = ?;";
    private final String readProprietarioQuery = "SELECT * FROM grupo WHERE nome = ? AND id_proprietario = ?";
    private final String updateQuery = "UPDATE grupo SET nome = ?, descricao = ? WHERE id = ?;";
    private final String deleteQuery = "DELETE FROM grupo WHERE id = ?";
    private final String allQuery = "SELECT * FROM grupo";
    private final String recoverGruposPertence = "SELECT * FROM grupo g, grupo_pertence gp WHERE g.id = gp.id_grupo AND gp.id_usuario = ?";
    
    public GrupoDAO(Connection connection){
        super(connection);
    }
    
    @Override
    public void create(Grupo grupo){
        try(PreparedStatement statement = connection.prepareStatement(createQuery);){
            statement.setString(1, grupo.getNome());
            statement.setString(2, grupo.getDescricao());
            statement.setLong(3, grupo.getId_proprietario());
            
            statement.executeUpdate();
        }catch(SQLException e){
//            throw new RuntimeException(e);
              System.out.println(e.getMessage());
        }
    }

    @Override
    public Grupo read(Integer id){
        Grupo grupo = new Grupo();
        
        try(PreparedStatement statement = connection.prepareStatement(readQuery);){
            statement.setLong(1, id);
            
            try(ResultSet result = statement.executeQuery();){
                if(result.next()){
                    grupo.setId(result.getInt("id"));
                    grupo.setId_proprietario(result.getInt("id_proprietario"));
                    grupo.setNome(result.getString("nome"));
                    grupo.setDescricao(result.getString("descricao"));
                }
            }catch(SQLException e){
                throw new RuntimeException(e);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return grupo;
    }
    
    public Grupo readProprietario(String nome, Integer idProprietario){
        Grupo grupo = new Grupo();
        try(PreparedStatement statement = connection.prepareStatement(readProprietarioQuery);){
            statement.setString(1, nome);
            statement.setLong(2, idProprietario);
            
            try(ResultSet result = statement.executeQuery();){
                if(result.next()){
                    grupo.setId(result.getInt("id"));
                    grupo.setId_proprietario(result.getInt("id_proprietario"));
                    grupo.setNome(result.getString("nome"));
                    grupo.setDescricao(result.getString("descricao"));
                }
            }catch(SQLException e){
                throw new RuntimeException(e);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return grupo;
    }
    
    public Grupo readNome(String nome){
        Grupo grupo = new Grupo();
        try(PreparedStatement statement = connection.prepareStatement(readNomeQuery);){
            statement.setString(1, nome);
            
            try(ResultSet result = statement.executeQuery();){
                if(result.next()){
                    grupo.setId(result.getInt("id"));
                    grupo.setId_proprietario(result.getInt("id_proprietario"));
                    grupo.setNome(result.getString("nome"));
                    grupo.setDescricao(result.getString("descricao"));
                }else{
                    return null;
                }
            }catch(SQLException e){
                throw new RuntimeException(e);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        
        return grupo;
    }
    
    @Override
    public void update(Grupo grupo){
        try(PreparedStatement statement = connection.prepareStatement(updateQuery);){
            statement.setString(1, grupo.getNome());
            statement.setString(2, grupo.getDescricao());
            statement.setInt(3, grupo.getId());
            
            if (statement.executeUpdate() < 1) {
                throw new SQLException("Falha ao editar: grupo não encontrado.");
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public void delete(Integer id){
        try(PreparedStatement statement = connection.prepareStatement(deleteQuery);){
            statement.setInt(1, id);
            
            if(statement.executeUpdate() < 1){
                throw new SQLException("Falha ao excluir: grupo não encontrado");
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public List<Grupo> all(){
        List<Grupo> grupos = new ArrayList<>();
     
        try(PreparedStatement statement = connection.prepareStatement(allQuery);){
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Grupo grupo = new Grupo();
                grupo.setId(result.getInt("id"));
                grupo.setId_proprietario(result.getInt("id_proprietario"));
                grupo.setNome(result.getString("nome"));
                grupo.setDescricao(result.getString("descricao"));
                
                grupos.add(grupo);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return grupos;
    }
    
    public List<Grupo> recoverGruposPertence(Integer id){
        List<Grupo> grupos = new ArrayList<>();
     
        try(PreparedStatement statement = connection.prepareStatement(recoverGruposPertence);){
            statement.setLong(1, id);
            ResultSet result = statement.executeQuery();
            
            while(result.next()){
                Grupo grupo = new Grupo();
                grupo.setId(result.getInt("id"));
                grupo.setId_proprietario(result.getInt("id_proprietario"));
                grupo.setNome(result.getString("nome"));
                grupo.setDescricao(result.getString("descricao"));
                
                grupos.add(grupo);
            }
            
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
        return grupos;
    }
    
}
