package controller;

import Utils.Filtro;
import dao.ComentarioDAO;
import dao.DAOFactory;
import dao.HashtagDAO;
import dao.PostDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Comentario;
import model.Hashtag;
import model.Post;

@WebServlet(urlPatterns = {"/comentar",
                            "/comentario/create"
                        })
public class ComentarioController extends HttpServlet{
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ComentarioDAO dao;
        PostDAO postDAO;
        Comentario ligaComentario;
        Post comentario, post;
        RequestDispatcher dispatcher;
        HttpSession session;
        
        switch(request.getServletPath()){
            case "/comentar":
                session = request.getSession(false);
                Integer id_post = Integer.parseInt(request.getParameter("id_post"));
                try(DAOFactory daoFactory = new DAOFactory();){
                    postDAO = daoFactory.getPostDAO();
                    post = postDAO.read(id_post);
                    
                }catch(SQLException e){
                    dispatcher = request.getRequestDispatcher("/view/post/index.jsp");
                    dispatcher.forward(request, response);
                    break;
                }
                
                session.setAttribute("post", post);
                dispatcher = request.getRequestDispatcher("/view/post/comentario.jsp");
                dispatcher.forward(request, response);

                
                
                break;
        
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ComentarioDAO comentarioDAO;
        PostDAO postDAO;
        Comentario ligaComentario;
        Post comentario, post;
        RequestDispatcher dispatcher;
        HttpSession session;
        Hashtag hashtag;
        HashtagDAO hashtagDAO;
        Filtro filtro = new Filtro();
        List<String> tags = new ArrayList<>();
        
        switch (request.getServletPath()){
            case "/comentario/create":
                comentario = new Post();
                
                comentario.setTitulo("Comentario");
                if(request.getParameter("texto").equals("")){
                    response.sendRedirect(request.getContextPath() + "/post");
                    return;
                }
               
                comentario.setTexto(request.getParameter("texto"));
                comentario.setId_autor(Integer.parseInt(request.getParameter("id_autor")));
        
                try (DAOFactory daoFactory = new DAOFactory();) {
                    postDAO = daoFactory.getPostDAO();
                    
                    int id = postDAO.createRetornaID(comentario);
                    tags = filtro.buscarTags(comentario.getTexto(), filtro.HASHTAG);
                    if(tags.size() >0){
                        hashtagDAO = daoFactory.getHashtagDAO();
                        for (String tag : tags) {
                            hashtag = new Hashtag();
                            hashtag.setHashtag(tag);
                            hashtag.setId_post(id);
                            hashtagDAO.create(hashtag);
                        }
                    }
                    
                    comentarioDAO = daoFactory.getComentarioDAO();
                    ligaComentario = new Comentario();
                    ligaComentario.setId(id);
                    ligaComentario.setId_post(Integer.parseInt(request.getParameter("id_post")));
                    comentarioDAO.create(ligaComentario);
                    
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                
                response.sendRedirect(request.getContextPath() + "/post");

                break;
        }
        
    }
    
    
}
