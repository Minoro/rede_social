package controller;

import Utils.Filtro;
import dao.DAOFactory;
import dao.EstatisticaDAO;
import dao.UsuarioDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.PostEstatistica;
import model.Usuario;
import model.UsuarioEstatistica;
import model.UsuarioSimilar;



@WebServlet(urlPatterns = {"/top20",
                            "/estatisticas/top20",
                            "/top20_post",
                            "/estatisticas/top20_post",
                            "/estatisticas",
                            "/similaridade",
                            "/flutuancia",
                            "/estatistica/zona_influencia"
                        
                        })
public class EstatisticaController extends HttpServlet{
    EstatisticaDAO estatisticaDAO;
    RequestDispatcher dispatcher;
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            
            case "/estatisticas":
                dispatcher = request.getRequestDispatcher("/view/estatisticas/index.jsp");
                dispatcher.forward(request, response);
                
                break;
            
            case "/top20":
                dispatcher = request.getRequestDispatcher("/view/estatisticas/top20.jsp");
                dispatcher.forward(request, response);
                break;
                
            case "/top20_post":
                dispatcher = request.getRequestDispatcher("/view/estatisticas/top20_post.jsp");
                dispatcher.forward(request, response);
                break;
                
            case "/similaridade":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                  
                    List<UsuarioSimilar> usuarios= estatisticaDAO.listUsuariosSimilares(Integer.parseInt(request.getParameter("id")));
                    request.setAttribute("usuarios", usuarios);          
                    
                    dispatcher = request.getRequestDispatcher("/view/estatisticas/similaridade.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/");
                }
                break;
            
            case "/estatistica/zona_influencia":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    UsuarioDAO usuarioDAO = daoFactory.getUsuarioDAO();
                    
                    int id = Integer.parseInt(request.getParameter("id"));
                    
                    Usuario usuario = usuarioDAO.read(id);
                    int zona_influencia = estatisticaDAO.zonaInfluencia(id);
                    request.setAttribute("zona_influencia", zona_influencia);          
                    request.setAttribute("usuario", usuario);          
                    
                    dispatcher = request.getRequestDispatcher("/view/estatisticas/zona_influencia.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/");
                }
                
                break;
                
            case "/flutuancia":
                dispatcher = request.getRequestDispatcher("/view/estatisticas/flutuancia.jsp");
                dispatcher.forward(request, response);
                break;
        }
    
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/estatisticas/top20":
                
                try (DAOFactory daoFactory = new DAOFactory();) {
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    
                    String data_inicio = request.getParameter("data_inicio");
                    String data_fim = request.getParameter("data_fim");
                    String hora_inicio = request.getParameter("hora_inicio");
                    String hora_fim = request.getParameter("hora_fim");
                    
                    String intervalo_inicio = data_inicio+" "+hora_inicio;
                    String intervalo_fim = data_fim+" "+hora_fim;
                  
                    List<UsuarioEstatistica> usuariosTop20 = estatisticaDAO.listTop20(intervalo_inicio, intervalo_fim);
                    request.setAttribute("usuariosTop20", usuariosTop20);          
                    
                    dispatcher = request.getRequestDispatcher("/view/estatisticas/top20_lista.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/");
                }
                
                break;
            
            case "/estatisticas/top20_post":
                
                try (DAOFactory daoFactory = new DAOFactory();) {
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    
                    String data_inicio = request.getParameter("data_inicio");
                    String data_fim = request.getParameter("data_fim");
                    String hora_inicio = request.getParameter("hora_inicio");
                    String hora_fim = request.getParameter("hora_fim");
                    
                    String intervalo_inicio = data_inicio+" "+hora_inicio;
                    String intervalo_fim = data_fim+" "+hora_fim;
                    
                    List<PostEstatistica> postsTop20 = estatisticaDAO.listTop20Post(intervalo_inicio, intervalo_fim); 
                    Filtro filtro = new Filtro();
                    
                    for (PostEstatistica postEstatistica : postsTop20) {
                        postEstatistica.setTexto(filtro.colocarLinkHashTag(postEstatistica.getTexto(), request.getContextPath()+"/hashtag"));
                        postEstatistica.setTexto(filtro.tragarTags(postEstatistica.getTexto(), request.getContextPath()));
                    }
                    
                    request.setAttribute("postsTop20", postsTop20);          
                    
                    dispatcher = request.getRequestDispatcher("/view/estatisticas/top20_posts_lista.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/");
                }
                
                break;
            
            case "/flutuancia":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    List<Float> flutuacao1 = new ArrayList<>();
                    List<Float> flutuacao2 = new ArrayList<>();
                    List<Float> flutuacao3 = new ArrayList<>();
                    
                    estatisticaDAO = daoFactory.getEstatisticaDAO();
                    
                    String data_inicio = request.getParameter("data_inicio");
                    String data_fim = request.getParameter("data_fim");
                    String hora_inicio = request.getParameter("hora_inicio");
                    String hora_fim = request.getParameter("hora_fim");
                    
                    String intervalo_inicio = data_inicio+" "+hora_inicio;
                    String intervalo_fim = data_fim+" "+hora_fim;
                  
                    List<UsuarioEstatistica> usuariosTop3 = estatisticaDAO.listTop3(intervalo_inicio, intervalo_fim);
                    
                    
                    flutuacao1 = estatisticaDAO.calcularFlutuancia(usuariosTop3.get(0).getId(), intervalo_inicio, intervalo_fim);
                    flutuacao2 = estatisticaDAO.calcularFlutuancia(usuariosTop3.get(1).getId(), intervalo_inicio, intervalo_fim);
                    flutuacao3 = estatisticaDAO.calcularFlutuancia(usuariosTop3.get(2).getId(), intervalo_inicio, intervalo_fim);
                        
                    request.setAttribute("usuarios", usuariosTop3);
                    request.setAttribute("flutuacao1", flutuacao1);
                    request.setAttribute("flutuacao2", flutuacao2);
                    request.setAttribute("flutuacao3", flutuacao3);
                    
                    dispatcher = request.getRequestDispatcher("/view/estatisticas/flutuancia_list.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/");
                }
                
                break;
        }
    
    } 
}
