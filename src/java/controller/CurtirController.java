package controller;

import dao.CurtirDAO;
import dao.DAO;
import dao.DAOFactory;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Curtir;
import model.Usuario;


@WebServlet(urlPatterns = {"/curtir",
                        "/desfazer_curtir",
                        "/nao_curtir",
                        "/desfazer_nao_curtir",
                        "/inverter_curtir",
                        "/inverter_nao_curtir"})

public class CurtirController extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        CurtirDAO curtirDAO;
        Curtir curtir;
        RequestDispatcher dispatcher;
        HttpSession session = null;
        Usuario usuario;
        
        switch(request.getServletPath()){
            case "/curtir":
                session = request.getSession();
                usuario = (Usuario) session.getAttribute("usuario");
                System.out.println("ID usuario curtiu = " + usuario.getId());
                curtir = new Curtir();
                curtir.setId_post(Integer.parseInt(request.getParameter("id_post")));
                curtir.setId_usuario(usuario.getId());
                curtir.setTipo(curtir.CURTIR);
                
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getCurtirDAO();
                    
                    dao.create(curtir);
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                    dispatcher = request.getRequestDispatcher("/view/usuario/");
                    dispatcher.forward(request, response);
                }
                response.sendRedirect(request.getContextPath() +"/");
                break;
                
            case "/desfazer_curtir":
                session = request.getSession();
                usuario = (Usuario) session.getAttribute("usuario");
                
                try(DAOFactory daoFactory = new DAOFactory();){
                    curtirDAO = daoFactory.getCurtirDAO();
                    
                    curtirDAO.delete(Integer.parseInt(request.getParameter("id_post")), usuario.getId());
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                    dispatcher = request.getRequestDispatcher("/view/usuario/");
                    dispatcher.forward(request, response);
                }
                response.sendRedirect(request.getContextPath() +"/");
                
                break;
            case "/nao_curtir":
                session = request.getSession();
                usuario = (Usuario) session.getAttribute("usuario");
                
                curtir = new Curtir();
                curtir.setId_post(Integer.parseInt(request.getParameter("id_post")));
                curtir.setId_usuario(usuario.getId());
                curtir.setTipo(curtir.NAO_CURTIR);
                
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getCurtirDAO();
                    
                    dao.create(curtir);
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                    dispatcher = request.getRequestDispatcher("/view/usuario/");
                    dispatcher.forward(request, response);
                }
                response.sendRedirect(request.getContextPath() +"/");
                break;
                
            case "/inverter_curtir":
                session = request.getSession();
                usuario = (Usuario) session.getAttribute("usuario");
                
                try(DAOFactory daoFactory = new DAOFactory();){
                    curtirDAO = daoFactory.getCurtirDAO();
                    curtir = curtirDAO.read(Integer.parseInt(request.getParameter("id_post")), usuario.getId());
                    
                    curtir.setTipo(!curtir.getTipo());
                    curtirDAO.update(curtir);
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                    dispatcher = request.getRequestDispatcher("/view/usuario/");
                    dispatcher.forward(request, response);
                }
                response.sendRedirect(request.getContextPath() +"/");
                break;
                
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    
    }
}
