package controller;

import dao.DAOFactory;
import dao.RepublicacaoDAO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Republicacao;
import model.Usuario;

@WebServlet(urlPatterns = {"/republicar",
                            "/republicar/delete"})

public class RepublicacaoController extends HttpServlet{
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RepublicacaoDAO republicacaoDAO;
        Republicacao republicacao;
        Usuario usuario;
        HttpSession session;
        RequestDispatcher dispatcher;

        
        switch(request.getServletPath()){
            case "/republicar":
                try(DAOFactory daoFactory = new DAOFactory();){
                    republicacaoDAO = daoFactory.getRepublicacaoDAO();
                    
                    session = request.getSession(false);
                    usuario = (Usuario) session.getAttribute("usuario");
                    
                    republicacao = new Republicacao();
                    republicacao.setId_usuario_republicando(usuario.getId());
                    republicacao.setId_post(Integer.parseInt(request.getParameter("id_post")));
                    
                    republicacaoDAO.create(republicacao);
                    
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                    dispatcher = request.getRequestDispatcher("/view/usuario/");
                    dispatcher.forward(request, response);
                }
                response.sendRedirect(request.getContextPath() +"/");
                
                break;
                
            
        }
        
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RepublicacaoDAO republicacaoDAO;
        Republicacao republicacao;
        
        switch(request.getServletPath()){
            case "/republicar/delete":
                    try (DAOFactory daoFactory = new DAOFactory();) {
                        republicacaoDAO = daoFactory.getRepublicacaoDAO();

                        republicacao = new Republicacao();
                        republicacao.setId_post(Integer.parseInt(request.getParameter("id")));
                        republicacao.setId_usuario_republicando(Integer.parseInt(request.getParameter("id_usuario")));

                        republicacaoDAO.delete(republicacao);

                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                    }

                    response.sendRedirect(request.getContextPath() + "/usuario");

                    break;
        }
    }
}
