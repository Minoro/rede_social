package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.DAOFactory;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ComentarioParaJSON;
import model.Curtir;
import model.Post;
import model.Republicacao;
import model.Usuario;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;


@WebServlet(name = "WebserviceController", urlPatterns = {
            "/importar",
            "/exportar"
            })
public class WebserviceController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/importar":
                break;
            case "/exportar":
                Pacote pacote = new Pacote();
                
                try(DAOFactory factory = new DAOFactory();){
                    List<Post> postsDB = factory.getPostDAO().all();
                    for (Post postDB : postsDB) {
                        PostJSON postJSON = new PostJSON();
                        postJSON.id = postDB.getId();
                        postJSON.id_usuario = postDB.getId_autor();
                        postJSON.titulo = postDB.getTitulo();
                        postJSON.conteudo = postDB.getTexto();
                        postJSON.data = postDB.getData_postagem();
                        
                        pacote.posts.add(postJSON);
                    }
                    
                    List<Republicacao> repostDB = factory.getRepublicacaoDAO().all();
                    int i = 1;
                    for (Republicacao republicacao : repostDB) {
                        RepostJSON repostJSON = new RepostJSON();
                        repostJSON.id = i;
                        repostJSON.id_post = republicacao.getId_post();
                        repostJSON.id_usuario = republicacao.getId_usuario_republicando();
                        repostJSON.data = republicacao.getData_republicacao();
                        i++;
                        
                        pacote.reposts.add(repostJSON);
                    }
                    
                    List<Curtir> curtidas = factory.getCurtirDAO().all();
                    for (Curtir curtir : curtidas) {
                        LikeJSON likeJSON = new LikeJSON();
                        likeJSON.id_post = curtir.getId_post();
                        likeJSON.id_usuario = curtir.getId_usuario();
                        likeJSON.data = curtir.getData_curtir();
                        
                        if(curtir.getTipo() == true)
                            pacote.likes.add(likeJSON);
                        else
                            pacote.dislikes.add(likeJSON);
                        
                    }
                    
                    List<ComentarioParaJSON> comentarios = factory.getComentarioDAO().allComentario();
                    for (ComentarioParaJSON comentario : comentarios) {
                        ComentarioJSON comentarioJSON = new ComentarioJSON();
                        comentarioJSON.id = comentario.getId_comentario();
                        comentarioJSON.id_post = comentario.getId();
                        comentarioJSON.conteudo = comentario.getTexto();
                        comentarioJSON.id_usuario = comentario.getId_autor();
                        comentarioJSON.data = comentario.getData_postagem();
                     
                        pacote.comentarios.add(comentarioJSON);
                    }
                    
                    List<Usuario> usuarios = factory.getUsuarioDAO().all();
                    for (Usuario usuario : usuarios) {
                        UsuarioJSON usuarioJSON = new UsuarioJSON();
                        usuarioJSON.id = usuario.getId();
                        usuarioJSON.nome = usuario.getNome();
                        usuarioJSON.login = usuario.getLogin();
                        usuarioJSON.nascimento = usuario.getData_nascimento();
                        usuarioJSON.descricao = usuario.getDescricao();
                        
                        pacote.usuarios.add(usuarioJSON);
                    }
                    
                    
                    
                }catch(SQLException ex){
                    
                }
                
                Gson gson = new GsonBuilder().setPrettyPrinting().setDateFormat("dd/MM/yyyy HH:mm").create();
                response.setCharacterEncoding("utf-8");
                response.setContentType("application/json");
                try(PrintWriter out = response.getWriter()){
                    gson.toJson(pacote, out);
                }
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        switch(request.getServletPath()){
            case "/importar":
                importarPOST(request, response);
                break;
        
        }

    }    
    static Gson gson = new GsonBuilder()
        .setPrettyPrinting()
        .setDateFormat("dd/MM/yyyy HH:mm")
        .create();

    private void importarPOST(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String url = request.getParameter("url");
        Pacote pacote = pegarPacoteDaURL(url);
        
        for (UsuarioJSON usuarioJSON : pacote.usuarios) {
            Usuario usuario = new Usuario();
            usuario.setNome(usuarioJSON.nome);
            usuario.setLogin(usuarioJSON.login);
            usuario.setData_nascimento(new Timestamp(usuarioJSON.nascimento.getTime()));
         
            //colocar da factory
        }
        
        for(PostJSON postJSON : pacote.posts){
            Post post = new Post();
            post.setTitulo(postJSON.titulo);
            post.setTexto(postJSON.conteudo);
            post.setData_postagem(new Timestamp(postJSON.data.getTime()));
        }
        
        
    }

    private Pacote pegarPacoteDaURL(String url) throws IOException {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            try (CloseableHttpResponse jsonResponse = httpClient.execute(new HttpGet(url)); InputStreamReader jsonContent = new InputStreamReader(jsonResponse.getEntity().getContent(), "utf-8")) {
                
                return gson.fromJson(jsonContent, Pacote.class);
            }
        }
    }
    
    static class Pacote{
        
        int id_servidor = 1;
        
        List<PostJSON> posts;
        List<RepostJSON> reposts;
        List<UsuarioJSON> usuarios;
        List<ComentarioJSON> comentarios;
        List<LikeJSON> likes;
        List<LikeJSON> dislikes;
        
        public Pacote(){
            this.posts = new LinkedList<>();
            this.reposts = new LinkedList<>();
            this.likes = new LinkedList<>();
            this.comentarios = new LinkedList<>();
            this.usuarios = new LinkedList<>();
            this.dislikes = new LinkedList<>();
        }
        
    }
    
    static class PostJSON{
        int id;
        int id_usuario;
        String titulo;
        String conteudo;
        Date data;
        
    }
    
    static class RepostJSON{
        int id;
        int id_usuario;
        int id_post;
        String conteudo;
        Date data;
    }
    
    static class LikeJSON{
        int id_post;
        int id_usuario;
        Date data;
    }
    
    static class ComentarioJSON{
        int id;
        int id_post;
        int id_usuario;
        String conteudo;
        Date data;
    }
    
    static class UsuarioJSON{
        int id;
        String nome;
        String login;
        Date nascimento;
        String descricao;
    }
    
}
