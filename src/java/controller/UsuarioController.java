package controller;

import Utils.Filtro;
import dao.CurtirDAO;
import dao.DAO;
import dao.DAOFactory;
import dao.GrupoDAO;
import dao.PostComUsuarioDAO;
import dao.PostDAO;
import dao.RepublicacaoDAO;
import dao.SeguidorDAO;
import dao.UsuarioDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Curtir;
import model.Grupo;
import model.Post;
import model.PostComUsuario;
import model.Republicacao;
import model.Seguidor;
import model.Usuario;

@WebServlet(urlPatterns = {"/usuario/create",
                           "/usuario/read",
                           "/usuario/update",
                           "/usuario/delete",
                           "/usuario/postar",
                           "/usuario/buscar",
                           "/usuario"})
@MultipartConfig(fileSizeThreshold=1024*1024*10,    // 10 MB
                 maxFileSize=1024*1024*50,      // 50 MB
                 maxRequestSize=1024*1024*100)  //100MB
public class UsuarioController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        UsuarioDAO usuarioDAO;
        SeguidorDAO seguidorDAO;
        RepublicacaoDAO republicacaoDAO;
        RequestDispatcher dispatcher;
        HttpSession session;
        Republicacao republicacao;
        Filtro filtro = new Filtro();
        
        switch (request.getServletPath()) {
            case "/usuario/create":
                dispatcher = request.getRequestDispatcher("/view/usuario/create.jsp");
                dispatcher.forward(request, response);

                break;
                
            case "/usuario/read":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();
                    republicacaoDAO = daoFactory.getRepublicacaoDAO();
                    session = request.getSession();
                    Usuario usuario = null;
                    
                    //Tratar quando se passa o ID ou quando não se passa
                    if(request.getParameter("id") != null){
//                        usuario = (Usuario) dao.read(Integer.parseInt(request.getParameter("id")));
                        //carrega posts do usuario
                        usuario = (Usuario) session.getAttribute("usuario");
                        
                        PostDAO postDAO = daoFactory.getPostDAO();
                        PostComUsuarioDAO postUsuarioDAO = daoFactory.getPostComUsuarioDAO();
//                        List<Post> postList = postDAO.allPost(Integer.parseInt(request.getParameter("id")));
                        List<PostComUsuario> postList = postUsuarioDAO.allPost(Integer.parseInt(request.getParameter("id")));
                        
                        List<Integer> statusRepublicacao = new ArrayList<>();
                        List<Integer> statusCurtir = new ArrayList<>();
                        List<Integer> curtidas = new ArrayList<>();
                        List<Integer> naoCurtidas = new ArrayList<>();
                        
                        
//                        List<List<Post>> comentarios = new ArrayList<>();
                        List<List<PostComUsuario>> comentarios = new ArrayList<>();
//                        ComentarioDAO comentarioDAO = daoFactory.getComentarioDAO();
                        PostComUsuarioDAO comentarioDAO = daoFactory.getPostComUsuarioDAO();
                        
                        CurtirDAO curtirDAO = daoFactory.getCurtirDAO(); 
                        Curtir curtir;
                        for (Post post : postList){
                            post.setTexto(filtro.colocarLinkHashTag(post.getTexto(), request.getContextPath()+"/hashtag"));
                            post.setTexto(filtro.tragarTags(post.getTexto(), request.getContextPath()));
                            
//                            List<Post> comentariosDoPost = new ArrayList<>();
                            List<PostComUsuario> comentariosDoPost = new ArrayList<>();
                        
//                            comentariosDoPost = comentarioDAO.allComentarios(post.getId());
                            comentariosDoPost = comentarioDAO.allComentarios(post.getId());
                            for (Post c : comentariosDoPost) {
                                c.setTexto(filtro.colocarLinkHashTag(c.getTexto(), request.getContextPath()+"/hashtag"));
                                c.setTexto(filtro.tragarTags(c.getTexto(), request.getContextPath()));
                            }

                            comentarios.add(comentariosDoPost);
                            
                            
                            curtir = curtirDAO.read(post.getId(), usuario.getId());
                            int numCurtidas = curtirDAO.countCurtir(post.getId());
                            int numNaoCurtidas = curtirDAO.naoCountCurtir(post.getId());
                            curtidas.add(numCurtidas);
                            naoCurtidas.add(numNaoCurtidas);
                            
                            if(curtir == null){
                                statusCurtir.add(0);
                            }else if(curtir.getTipo() == curtir.CURTIR){
                                statusCurtir.add(1);
                            }else if(curtir.getTipo() == curtir.NAO_CURTIR){
                                statusCurtir.add(2);
                            }
                            
                            republicacao = republicacaoDAO.read(post.getId(), usuario.getId());
                            
                            if(republicacao == null){
                                statusRepublicacao.add(0);
                            }else{
                                statusRepublicacao.add(1);
                            }
                            
                        }
                        usuario = (Usuario) dao.read(Integer.parseInt(request.getParameter("id")));
                        
                        request.setAttribute("comentarioList", comentarios);
                        session.setAttribute("statusCurtir", statusCurtir);
                        session.setAttribute("postList", postList);   
                        session.setAttribute("curtidas", curtidas);   
                        session.setAttribute("naoCurtidas", naoCurtidas);
                        session.setAttribute("statusRepublicacao", statusRepublicacao);
                        
                    }else{
                        usuario = (Usuario) session.getAttribute("usuario");
                    }
                    request.setAttribute("u", usuario);

                    
                    dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/usuario");
                }

                break;
            case "/usuario/update":
                dispatcher = request.getRequestDispatcher("/view/usuario/update.jsp");
                dispatcher.forward(request, response);
                
                break;
               
            case "/usuario/delete":
                //redireciona para pagina com o método POST
                dispatcher = request.getRequestDispatcher("/view/usuario/delete.jsp");
                dispatcher.forward(request, response);
                break;
                
            case "/usuario/buscar":
                try (DAOFactory daoFactory = new DAOFactory();) { 
                    usuarioDAO = daoFactory.getUsuarioDAO();
                    
                    session = request.getSession();
                    Usuario usuario = (Usuario) session.getAttribute("usuario");
                    
                    String busca = "%"+request.getParameter("usuario_buscado")+"%";
                    
                    List<Usuario> usuarioList = usuarioDAO.buscar(busca);
               
                    seguidorDAO = daoFactory.getSeguidorDAO();
                    List<Integer> estaSeguindoList = new ArrayList<Integer>();
                    Seguidor seguidor;
                    for(Usuario u : usuarioList){
                        seguidor = seguidorDAO.read(u.getId(),usuario.getId());
                        if(seguidor.getId_seguido() != null)
                            estaSeguindoList.add(1);
                        else
                            estaSeguindoList.add(0);
                    }
                    
                    request.setAttribute("usuarioList", usuarioList);
                    request.setAttribute("estaSeguindoList", estaSeguindoList);

                    dispatcher = request.getRequestDispatcher("/view/usuario/resultado_busca.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/usuario");
                }
                
                
                break;
            case "/usuario":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    usuarioDAO = daoFactory.getUsuarioDAO();
                    
                    session = request.getSession();
                    Usuario usuario = (Usuario) session.getAttribute("usuario");

                    List<Usuario> seguidosList = usuarioDAO.allSeguidos(usuario.getId());
                    List<Usuario> naoSeguidosList = usuarioDAO.allNaoSeguidos(usuario.getId());
                    
                    request.setAttribute("seguidosList", seguidosList);
                    request.setAttribute("naoSeguidosList", naoSeguidosList);
                    
                    dispatcher = request.getRequestDispatcher("/view/usuario/index.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/");
                }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        GrupoDAO grupoDAO;
        HttpSession session;
        Usuario usuario = new Usuario();
        String applicationPath = request.getServletContext().getRealPath("");
        String uploadFilePath = applicationPath + File.separator + "foto_usuario";
        Part filePart;
        String fileName;
        InputStream fileContent;
        FileOutputStream fileOut;
        File fileSaveDir = new File(uploadFilePath);
        int buffer;
        
        switch (request.getServletPath()) {
            case "/usuario/create":
                System.out.println(applicationPath + File.separator + "foto_usuario"); 
                
                usuario.setLogin(request.getParameter("login"));
                usuario.setSenha(request.getParameter("senha"));
                usuario.setNome(request.getParameter("nome"));
                usuario.setDescricao(request.getParameter("descricao"));
                if(request.getParameter("data_nascimento").equals("")){
//                    usuario.setData_nascimento(Date.valueOf(LocalDate.now()));
                    usuario.setData_nascimento(new Timestamp(Date.valueOf(LocalDate.now()).getTime()));
                }else{
                    usuario.setData_nascimento(new Timestamp(Date.valueOf(request.getParameter("data_nascimento")).getTime()));
                }
                
                if(!getFileName(request.getPart("foto")).equals("")){
                    salvarFoto(request, usuario);
                }else{
                    usuario.setFoto("default.jpg");
                }
                
                try (DAOFactory daoFactory = new DAOFactory();) {
                    grupoDAO = daoFactory.getGrupoDAO();
                    Grupo grupo = grupoDAO.readNome(usuario.getNome());
                    if(grupo == null){
                        dao = daoFactory.getUsuarioDAO();
                        dao.create(usuario);
                  
                    }
                    
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/"    );

                break;
                
            case "/usuario/update":
                usuario.setId(Integer.parseInt(request.getParameter("id")));
                usuario.setLogin(request.getParameter("login"));
                usuario.setNome(request.getParameter("nome"));
                usuario.setDescricao(request.getParameter("descricao"));
                usuario.setFoto(request.getParameter("foto_antiga"));
                usuario.setData_nascimento(new Timestamp(Date.valueOf(request.getParameter("data_nascimento")).getTime()));

                if(!getFileName(request.getPart("foto")).equals("")){
                    salvarFoto(request, usuario);
//                    usuario.setFoto(usuario.getLogin()+".jpg");
                }else{
                    usuario.setFoto(request.getParameter("foto_antiga"));
                }
                
                if (!request.getParameter("senha").isEmpty()) {
                    usuario.setSenha(request.getParameter("senha"));
                }

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();

                    dao.update(usuario);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                session = request.getSession(false);
                session.setAttribute("usuario", usuario);
                response.sendRedirect(request.getContextPath() + "/welcome.jsp");

                break;
                
            case "/usuario/delete":
                
                session = request.getSession(false);

                if (session != null) {
                    session.invalidate();
                }

                response.sendRedirect(request.getContextPath() + "/");
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getUsuarioDAO();
                    dao.delete(Integer.parseInt(request.getParameter("id")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                
                break;

        }
    }
    
    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= "+contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
    
    private void salvarFoto(HttpServletRequest request, Usuario usuario) throws ServletException, IOException{
        String applicationPath = request.getServletContext().getRealPath("");
        String uploadFilePath = applicationPath + File.separator + "foto_usuario";
        Part filePart;
        String fileName;
        InputStream fileContent;
        FileOutputStream fileOut;
        File fileSaveDir = new File(uploadFilePath);
        int buffer;
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
        filePart = request.getPart("foto");
        fileName = usuario.getLogin()+".jpg";
        fileContent = filePart.getInputStream();
        fileOut = new FileOutputStream(fileSaveDir+File.separator+fileName);
        while((buffer = fileContent.read()) > -1){
            fileOut.write(buffer);
        }
        fileContent.close();
        fileOut.close();

        usuario.setFoto(fileName);
    }
}