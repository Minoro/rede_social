package controller;

import dao.DAO;
import dao.DAOFactory;
import dao.GrupoDAO;
import dao.GrupoPertenceDAO;
import dao.UsuarioDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Grupo;
import model.GrupoPertence;
import model.Usuario;

@WebServlet(urlPatterns = {"/grupo/create",
                        "/grupo/read",
                        "/grupo/convidar",
                        "/grupo/update",
                        "/grupo/delete",
                        "/grupo/remover_membro",
                        "/grupo/meus_grupos",
                        "/grupo/editar",
                        "/grupo/todos",
                       "/grupo"})
public class GrupoController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        RequestDispatcher dispatcher;
        HttpSession session = null;
        
        switch (request.getServletPath()){
            case "/grupo/create":
                dispatcher = request.getRequestDispatcher("/view/grupo/create.jsp");
                dispatcher.forward(request, response);
                break;
                
            case "/grupo/read":
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getGrupoDAO();
                    
                    Grupo grupo = (Grupo) dao.read(Integer.parseInt(request.getParameter("id")));
                    request.setAttribute("grupo", grupo);
                    
                    GrupoPertenceDAO daoGrupoPertence = daoFactory.getGrupoPertenceDAO();
                    
                     List<Usuario> membros = 
                            daoGrupoPertence.allMembrosGrupo(Integer.parseInt(request.getParameter("id")));
                    request.setAttribute("membros", membros);
                    
                    dispatcher = request.getRequestDispatcher("/view/grupo/read.jsp");
                    dispatcher.forward(request, response);
                    
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                    dispatcher = request.getRequestDispatcher("/view/grupo/");
                    dispatcher.forward(request, response);
                }
                
                break;
                
            case "/grupo/meus_grupos":
                try(DAOFactory daoFactory = new DAOFactory();){
                    GrupoDAO daoGrupo = daoFactory.getGrupoDAO();
                    
                    List<Grupo> grupoParticipaList = 
                            daoGrupo.recoverGruposPertence(Integer.parseInt(request.getParameter("id")));
                    request.setAttribute("grupoParticipaList", grupoParticipaList);
                    
                }catch(SQLException e){
                    throw new RuntimeException(e);
                }
                
                dispatcher = request.getRequestDispatcher("/view/grupo/meus_grupos.jsp");
                dispatcher.forward(request, response);
                break;
                
            case "/grupo/convidar":
                try(DAOFactory daoFactory = new DAOFactory();){
                    GrupoDAO daoGrupo = daoFactory.getGrupoDAO();
                    Grupo grupo = daoGrupo.read(Integer.parseInt(request.getParameter("id")));
                    request.setAttribute("grupo", grupo);
                                       
                    GrupoPertenceDAO daoGrupoPertence = daoFactory.getGrupoPertenceDAO();
                    List<Usuario> usuarioList = daoGrupoPertence.allMembrosForaDoGrupo(Integer.parseInt(request.getParameter("id")));
                    
                    request.setAttribute("usuarioList", usuarioList);
                                 
                }catch(SQLException e){
                    throw new RuntimeException(e);
                }
                
                dispatcher = request.getRequestDispatcher("/view/grupo/convidar.jsp");
                dispatcher.forward(request, response);
                break;
                
            case "/grupo/todos":
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getGrupoDAO();
                    
                    List<Grupo> grupos = dao.all();
                    request.setAttribute("grupos", grupos);
                    
                }catch(SQLException e){
                    System.out.println(e.getMessage());
                    response.sendRedirect(request.getContextPath()+ "/grupo");
                }
                dispatcher = request.getRequestDispatcher("/view/grupo/todos.jsp");
                dispatcher.forward(request, response);
                break;
                
            case "/grupo":
                dispatcher = request.getRequestDispatcher("/view/grupo/index.jsp");
                dispatcher.forward(request, response);
                
                break;
        
        }
        
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        GrupoDAO dao;
        UsuarioDAO usuarioDAO;
        Usuario usuario;
        RequestDispatcher dispatcher;
        Grupo grupo = new Grupo();
        GrupoPertence grupoPertence = new GrupoPertence();
        GrupoPertenceDAO daoGrupoPertence;
        String usuarios[];
        
        switch (request.getServletPath()){
            case "/grupo/create":
                
                if(request.getParameter("nome").equals("")){
                    grupo.setNome("Grupo Sem Nome");
                    if(request.getParameter("descricao").equals("")){
                        response.sendRedirect(request.getContextPath()+ "/grupo");
                        return;
                    }
                }else{
                    grupo.setNome(request.getParameter("nome"));
                }
                grupo.setDescricao(request.getParameter("descricao"));
                grupo.setId_proprietario(Integer.parseInt(request.getParameter("id_proprietario")));
                
                try(DAOFactory daoFactory = new DAOFactory();){                    
                    
                    daoGrupoPertence = daoFactory.getGrupoPertenceDAO();
                    dao = daoFactory.getGrupoDAO();
                    
                    usuarioDAO = daoFactory.getUsuarioDAO();
                    usuario = usuarioDAO.buscarPorLogin(grupo.getNome());
                    if(usuario != null){
                        response.sendRedirect(request.getContextPath() + "/grupo");
                    }
                    
                    dao.create(grupo);
                    
                    grupo = dao.readProprietario(grupo.getNome(), grupo.getId_proprietario());
                    grupoPertence.setId_grupo(grupo.getId());
                    grupoPertence.setId_usuario(grupo.getId_proprietario());
                    daoGrupoPertence.create(grupoPertence);
                            
                }catch(SQLException e){
                    System.out.println(e.getMessage());
                    response.sendRedirect(request.getContextPath()+ "/grupo");
                }
                
                response.sendRedirect(request.getContextPath() + "/grupo");
                
                break;
           
            case "/grupo/update":
                grupo.setId(Integer.parseInt(request.getParameter("id")));

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getGrupoDAO();

                    grupo = (Grupo) dao.read(Integer.parseInt(request.getParameter("id")));
                    
                    request.setAttribute("grupo", grupo);

                    dispatcher = request.getRequestDispatcher("/view" + "/grupo/update" + ".jsp");
                    dispatcher.forward(request, response);

                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/grupo/meus_grupos");
                }
                break;
                
            case "/grupo/editar":
                grupo.setId(Integer.parseInt(request.getParameter("id")));
                grupo.setId_proprietario(Integer.parseInt(request.getParameter("id_proprietario")));
                grupo.setNome(request.getParameter("nome"));
                grupo.setDescricao(request.getParameter("descricao"));
                
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getGrupoDAO();
                    dao.update(grupo);
                    
                }catch(SQLException e){
                    throw new RuntimeException(e);
                }
                response.sendRedirect(request.getContextPath()+ "/grupo/meus_grupos?id="+request.getParameter("id_proprietario"));
                
                break;
                
            case "/grupo/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getGrupoDAO();
                    dao.delete(Integer.parseInt(request.getParameter("id")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath()+ "/grupo/meus_grupos?id="+request.getParameter("id_usuario"));

                break;
                
            case "/grupo/remover_membro":
                usuarios = request.getParameterValues("delete");

                try (DAOFactory daoFactory = new DAOFactory();) {
                    daoGrupoPertence = daoFactory.getGrupoPertenceDAO();

                    try {
                        daoFactory.beginTransaction(); grupoPertence.setId_grupo(Integer.parseInt(request.getParameter("id_grupo")));
                        for (int i = 0; i < usuarios.length; ++i) {
                            daoGrupoPertence.delete(Integer.parseInt(request.getParameter("id_grupo")), 
                                                                    Integer.parseInt(usuarios[i]));
                        }

                        daoFactory.commitTransaction();
                        daoFactory.endTransaction();
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        daoFactory.rollbackTransaction();
                    }
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                response.sendRedirect(request.getContextPath() + "/grupo");
                
                break;
                
            case "/grupo/convidar":
                usuarios = request.getParameterValues("convidar");

                try (DAOFactory daoFactory = new DAOFactory();) {
                    daoGrupoPertence = daoFactory.getGrupoPertenceDAO();

                    try {
                        daoFactory.beginTransaction();
                        grupoPertence.setId_grupo(Integer.parseInt(request.getParameter("id_grupo")));
                        for (int i = 0; i < usuarios.length; ++i) {
                            grupoPertence.setId_usuario(Integer.parseInt(usuarios[i]));
                            daoGrupoPertence.create(grupoPertence);
                        }

                        daoFactory.commitTransaction();
                        daoFactory.endTransaction();
                    } catch (SQLException ex) {
                        System.err.println(ex.getMessage());
                        daoFactory.rollbackTransaction();
                    }
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                response.sendRedirect(request.getContextPath() + "/grupo");
                
                break;    
                
        }
    
    }
}
