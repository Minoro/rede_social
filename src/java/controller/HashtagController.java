package controller;

import Utils.Filtro;
import dao.DAOFactory;
import dao.HashtagDAO;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Post;

@WebServlet(urlPatterns = {"/hashtag/hashtag",
                            "/hashtag"})
public class HashtagController extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HashtagDAO dao;
        RequestDispatcher dispatcher;
        Filtro filtro = new Filtro();
        switch(request.getServletPath()){
            case "/hashtag/hashtag":
                List<Post> posts = new ArrayList<>();
                System.out.println("ASDAAS = " + request.getParameter("tag"));
                 try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getHashtagDAO();
                    
                    posts = dao.postHashtag(request.getParameter("tag"));
                    
                     for (Post post : posts) {     
                        post.setTexto(filtro.colocarLinkHashTag(post.getTexto(), request.getContextPath()+"/hashtag"));
                    
                     }
                }catch(SQLException e){
                    throw new RuntimeException(e);
                }
                request.setAttribute("posts", posts);
                dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                dispatcher.forward(request, response);
                
                break;
        
        }
    
    }
}
