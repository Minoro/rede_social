package controller;

import Utils.Filtro;
import dao.ComentarioDAO;
import dao.CurtirDAO;
import dao.DAO;
import dao.DAOFactory;
import dao.HashtagDAO;
import dao.PostComUsuarioDAO;
import dao.PostDAO;
import dao.RepublicacaoDAO;
import dao.SeguidorDAO;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.Curtir;
import model.Hashtag;
import model.Post;
import model.PostComUsuario;
import model.Republicacao;
import model.Seguidor;
import model.Usuario;


@WebServlet(urlPatterns = {"/post/create",
                       "/post/read",
                       "/post/update",
                       "/post/delete",
                       "/post/postar",
                       "/post/editar",
                       "/post/minhas_postagens",
                       "/post/post_seguidos",
                       "/post/buscar",
                       "/post/arquivo",
                       "/post"})
@MultipartConfig(fileSizeThreshold=1024*1024*10,    // 10 MB
                 maxFileSize=1024*1024*50,      // 50 MB
                 maxRequestSize=1024*1024*100)  //100MB
public class PostController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        RequestDispatcher dispatcher;
        HttpSession session = null;
        Post post = null;
        Filtro filtro = new Filtro();
        Republicacao republicacao;
        CurtirDAO curtirDAO;
        Curtir curtir;
        RepublicacaoDAO republicacaoDAO;

        switch (request.getServletPath()) {
            case "/post/create":
                dispatcher = request.getRequestDispatcher("/view/post/create.jsp");
                dispatcher.forward(request, response);

                break;
            case "/post/read":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getPostDAO();
                    post = (Post) dao.read(Integer.parseInt(request.getParameter("id")));
                   
                }catch(SQLException e){
                    throw new RuntimeException(e);
                }
                request.setAttribute("post", post);
                dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                dispatcher.forward(request, response);
                    
                break;
                
            case "/post/update":
                dispatcher = request.getRequestDispatcher("/view/post/editar.jsp");
                dispatcher.forward(request, response);

                break;
            case "/post/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getPostDAO();
                    dao.delete(Integer.parseInt(request.getParameter("id")));
                    
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }

                response.sendRedirect(request.getContextPath() + "/usuario");

                break;
                
            case "/post":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dispatcher = request.getRequestDispatcher("/view/post/index.jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/post");
                }
                break;
            
            case "/post/minhas_postagens":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    PostDAO postDAO = daoFactory.getPostDAO();
                    ComentarioDAO comentarioDAO = daoFactory.getComentarioDAO();
                    PostComUsuarioDAO postUsuarioDAO = daoFactory.getPostComUsuarioDAO();
                    session = request.getSession();
                    Usuario usuario = (Usuario) session.getAttribute("usuario");
                    
//                    List<Post> postList = postDAO.allPost(usuario.getId());
                    List<PostComUsuario> postList = postUsuarioDAO.allPost(usuario.getId());
                    
//                    List<List<Post>> comentarios = new ArrayList<>();
                    List<List<PostComUsuario>> comentarios = new ArrayList<>();
                    
                    
                    List<Integer> curtidas = new ArrayList<>();
                    List<Integer> naoCurtidas = new ArrayList<>();
                    republicacaoDAO = daoFactory.getRepublicacaoDAO();
                    List<Integer> statusCurtir = new ArrayList<>();
                       
                    curtirDAO = daoFactory.getCurtirDAO();
                    for (Post p : postList){
                        
                        p.setTexto(filtro.colocarLinkHashTag(p.getTexto(), request.getContextPath()+"/hashtag"));
                        p.setTexto(filtro.tragarTags(p.getTexto(), request.getContextPath()));
                        
                        List<PostComUsuario> comentariosDoPost = new ArrayList<>();
//                        List<Post> comentariosDoPost = new ArrayList<>();
                        
//                        comentariosDoPost = comentarioDAO.allComentarios(p.getId());
                        comentariosDoPost = postUsuarioDAO.allComentarios(p.getId());
                        for (Post c : comentariosDoPost) {
                            c.setTexto(filtro.colocarLinkHashTag(c.getTexto(), request.getContextPath()+"/hashtag"));
                            c.setTexto(filtro.tragarTags(c.getTexto(), request.getContextPath()));
                        }
                        comentarios.add(comentariosDoPost);
                        
                        curtir = curtirDAO.read(p.getId(), usuario.getId());
                        if(curtir == null){
                            statusCurtir.add(0);
                        }else if(curtir.getTipo() == curtir.CURTIR){
                            statusCurtir.add(1);
                        }else if(curtir.getTipo() == curtir.NAO_CURTIR){
                            statusCurtir.add(2);
                        }
                        
                        int numCurtidas = curtirDAO.countCurtir(p.getId());
                        int numNaoCurtidas = curtirDAO.naoCountCurtir(p.getId());
                        curtidas.add(numCurtidas);
                        naoCurtidas.add(numNaoCurtidas);
                  
                        session.setAttribute("statusCurtir", statusCurtir);
                        session.setAttribute("curtidas", curtidas);   
                        session.setAttribute("naoCurtidas", naoCurtidas);
                        
                    }
                    request.setAttribute("postList", postList);
                    request.setAttribute("comentarioList", comentarios);
                    
                    dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/post");
                }
                break;
                
            case "/post/post_seguidos":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    PostDAO postDAO = daoFactory.getPostDAO();
                    SeguidorDAO seguidorDAO = daoFactory.getSeguidorDAO();
                    ComentarioDAO comentarioDAO = daoFactory.getComentarioDAO();
                    
                    session = request.getSession();
                    Usuario usuario = (Usuario) session.getAttribute("usuario");
                    
                    List<Seguidor> seguidores = seguidorDAO.allSeguidos(usuario.getId());
//                    List<Post> postList = new ArrayList<>();
                    List<PostComUsuario> postList = new ArrayList<>();
//                    List<List<Post>> comentarios = new ArrayList<>();
                    List<List<PostComUsuario>> comentarios = new ArrayList<>();
                    
//                    for (Seguidor seguido : seguidores) {
//                        List<Post> postSeguido = postDAO.allPostAutor(seguido.getId_seguido());
//                        postList.addAll(postSeguido);
//                    }
                    
                    PostComUsuarioDAO postUsuarioDAO = daoFactory.getPostComUsuarioDAO();
                    for (Seguidor seguido : seguidores) {
                        List<PostComUsuario> postSeguido = postUsuarioDAO.allPostAutor(seguido.getId_seguido());
//                        List<Post> postSeguido = postDAO.allPostAutor(seguido.getId_seguido());
                        postList.addAll(postSeguido);
                    }
                    
                    List<Integer> curtidas = new ArrayList<>();
                    List<Integer> naoCurtidas = new ArrayList<>();
                    republicacaoDAO = daoFactory.getRepublicacaoDAO();
                    List<Integer> statusCurtir = new ArrayList<>();
                    
                    curtirDAO = daoFactory.getCurtirDAO();
                    for (Post p : postList){
                        p.setTexto(filtro.colocarLinkHashTag(p.getTexto(), request.getContextPath()+"/hashtag"));
                        p.setTexto(filtro.tragarTags(p.getTexto(), request.getContextPath()));
                        
//                        List<Post> comentariosDoPost = new ArrayList<>();
                        List<PostComUsuario> comentariosDoPost = new ArrayList<>();
                        
                        comentariosDoPost = postUsuarioDAO.allComentarios(p.getId());
//                        comentariosDoPost = comentarioDAO.allComentarios(p.getId());
                        for (Post c : comentariosDoPost) {
                            c.setTexto(filtro.colocarLinkHashTag(c.getTexto(), request.getContextPath()+"/hashtag"));
                            c.setTexto(filtro.tragarTags(c.getTexto(), request.getContextPath()));
                        }
                        
                        comentarios.add(comentariosDoPost);
                        
                        
                        curtir = curtirDAO.read(p.getId(), usuario.getId());
                        if(curtir == null){
                            statusCurtir.add(0);
                        }else if(curtir.getTipo() == curtir.CURTIR){
                            statusCurtir.add(1);
                        }else if(curtir.getTipo() == curtir.NAO_CURTIR){
                            statusCurtir.add(2);
                        }
                        
                        int numCurtidas = curtirDAO.countCurtir(p.getId());
                        int numNaoCurtidas = curtirDAO.naoCountCurtir(p.getId());
                        curtidas.add(numCurtidas);
                        naoCurtidas.add(numNaoCurtidas);
                  
                        session.setAttribute("statusCurtir", statusCurtir);
                        session.setAttribute("curtidas", curtidas);   
                        session.setAttribute("naoCurtidas", naoCurtidas);
                        
                    }
                    
                    request.setAttribute("postList", postList);
                    request.setAttribute("comentarioList", comentarios);
                    
                    dispatcher = request.getRequestDispatcher("/view" + request.getServletPath() + ".jsp");
                    dispatcher.forward(request, response);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/post");
                }

                break;
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostDAO dao;
        HashtagDAO hashtagDAO;
        HttpSession session;
        Filtro filtro = new Filtro();
        List<String> tags = new ArrayList<>();
        Post post = new Post();
        Hashtag hashtag;
        RequestDispatcher dispatcher;
        Republicacao republicacao;
        CurtirDAO curtirDAO;
        Curtir curtir;
        RepublicacaoDAO republicacaoDAO;
        ComentarioDAO comentarioDAO;

        switch (request.getServletPath()){
            case "/post/create":
                if(request.getParameter("titulo").equals("")){
                    post.setTitulo("Postagem Sem Título");
                    if(request.getParameter("texto").equals("")){
                        response.sendRedirect(request.getContextPath() + "/post");
                        return;
                    }
                }else{
                    post.setTitulo(request.getParameter("titulo"));
                }
                post.setTexto(request.getParameter("texto"));
                post.setId_autor(Integer.parseInt(request.getParameter("id_autor")));
                
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getPostDAO();
                    
                    int id = dao.createRetornaID(post);
                    tags = filtro.buscarTags(post.getTexto(), filtro.HASHTAG);
                    if(tags.size() >0){
                        hashtagDAO = daoFactory.getHashtagDAO();
                        for (String tag : tags) {
                            hashtag = new Hashtag();
                            hashtag.setHashtag(tag);
                            hashtag.setId_post(id);
                            hashtagDAO.create(hashtag);
                        }
                    }
                    
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
                
                response.sendRedirect(request.getContextPath() + "/post");

                break;
                
            case "/post/update":
                post.setId(Integer.parseInt(request.getParameter("id")));

                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getPostDAO();

                    post = (Post) dao.read(Integer.parseInt(request.getParameter("id")));
                    tags = filtro.buscarTags(post.getTexto(), filtro.HASHTAG);
                    if(tags.size() > 0){
                        hashtagDAO = daoFactory.getHashtagDAO();
                        hashtagDAO.delete(post.getId());
                    }
                    request.setAttribute("post", post);

                    dispatcher = request.getRequestDispatcher("/view" + "/post/editar" + ".jsp");
                    dispatcher.forward(request, response);

                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath() + "/post/minhas_postagens");
                }

                break;
                
            case "/post/editar":
                post.setId(Integer.parseInt(request.getParameter("id")));
                post.setId_autor(Integer.parseInt(request.getParameter("id_autor")));
                post.setTexto(request.getParameter("texto"));
                post.setTitulo(request.getParameter("titulo"));
                
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getPostDAO();
                    
                    dao.update(post);
                    
                    if(post.getTexto().matches(filtro.HASHTAG)){
                        tags = filtro.buscarTags(post.getTexto(), filtro.HASHTAG);
                        if(tags.size() > 0){
                            hashtagDAO = daoFactory.getHashtagDAO();
                            hashtagDAO.delete(post.getId());
                            for (String tag : tags) {
                                hashtag = new Hashtag();
                                hashtag.setHashtag(tag);
                                hashtag.setId_post(post.getId());

                                hashtagDAO.create(hashtag);
                            }
                        }
                    }
                    
                }catch(SQLException e){
//                    throw new RuntimeException(e);
                    response.sendRedirect(request.getContextPath()+ "/post/");
                }
                response.sendRedirect(request.getContextPath()+ "/post/minhas_postagens");
                
                break;
            case "/post/delete":
                try (DAOFactory daoFactory = new DAOFactory();) {
                    dao = daoFactory.getPostDAO();
                    post = null;
                    post = (Post) dao.read(Integer.parseInt(request.getParameter("id")));
                    if(post.getTexto().matches(filtro.HASHTAG)){
                        hashtagDAO = daoFactory.getHashtagDAO();
                        hashtagDAO.delete(Integer.parseInt(request.getParameter("id")));
                    }
                    dao.delete(Integer.parseInt(request.getParameter("id")));
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                    response.sendRedirect(request.getContextPath()+ "/post");
                }

                response.sendRedirect(request.getContextPath()+ "/post/minhas_postagens");

                break;
                
            case "/post/buscar":
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getPostDAO();
                    comentarioDAO = daoFactory.getComentarioDAO();
                    session = request.getSession(false);
                    
                    Usuario usuario = (Usuario) session.getAttribute("usuario");
                    
                    String busca = request.getParameter("campo_busca");
                    System.out.println("Busca = " + busca);
                    busca = filtro.tratarBuscaDePost(busca);
                    
                    List<Post> postList = dao.buscarPost(busca);
                    
                    List<List<Post>> comentarios = new ArrayList<>();
                    
                    List<Integer> curtidas = new ArrayList<>();
                    List<Integer> naoCurtidas = new ArrayList<>();
                    republicacaoDAO = daoFactory.getRepublicacaoDAO();
                    List<Integer> statusCurtir = new ArrayList<>();
                    
                    curtirDAO = daoFactory.getCurtirDAO();
                    for (Post p : postList){
                        p.setTexto(filtro.colocarLinkHashTag(p.getTexto(), request.getContextPath()+"/hashtag"));
                        p.setTexto(filtro.tragarTags(p.getTexto(), request.getContextPath()));
                        
                        List<Post> comentariosDoPost = new ArrayList<>();
                        
                        comentariosDoPost = comentarioDAO.allComentarios(p.getId());
                        for (Post c : comentariosDoPost) {
                            c.setTexto(filtro.colocarLinkHashTag(c.getTexto(), request.getContextPath()+"/hashtag"));
                            c.setTexto(filtro.tragarTags(c.getTexto(), request.getContextPath()));
                        }
                        
                        comentarios.add(comentariosDoPost);
                        
                        
                        curtir = curtirDAO.read(p.getId(), usuario.getId());
                        if(curtir == null){
                            statusCurtir.add(0);
                        }else if(curtir.getTipo() == curtir.CURTIR){
                            statusCurtir.add(1);
                        }else if(curtir.getTipo() == curtir.NAO_CURTIR){
                            statusCurtir.add(2);
                        }
                        
                        int numCurtidas = curtirDAO.countCurtir(p.getId());
                        int numNaoCurtidas = curtirDAO.naoCountCurtir(p.getId());
                        curtidas.add(numCurtidas);
                        naoCurtidas.add(numNaoCurtidas);
                  
                        request.setAttribute("statusCurtir", statusCurtir);
                        request.setAttribute("curtidas", curtidas);   
                        request.setAttribute("naoCurtidas", naoCurtidas);
                        
                    }
                    
                    
                    
                    request.setAttribute("postList", postList);
                
                }catch(SQLException e){
                    System.err.println(e.getMessage());
                }
                
                dispatcher = request.getRequestDispatcher("/view/post/resultado_busca.jsp");
                dispatcher.forward(request, response);
                
                
                break;
                
            case "/post/arquivo":
                session = request.getSession(false);
                Usuario usuario = (Usuario) session.getAttribute("usuario");
                salvarFoto(request, usuario);
                
                
                break;
            
        }
        
    }
    
    
    private String getFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        System.out.println("content-disposition header= "+contentDisp);
        String[] tokens = contentDisp.split(";");
        for (String token : tokens) {
            if (token.trim().startsWith("filename")) {
                return token.substring(token.indexOf("=") + 2, token.length()-1);
            }
        }
        return "";
    }
    
    private void salvarFoto(HttpServletRequest request, Usuario usuario) throws ServletException, IOException{
        String applicationPath = request.getServletContext().getRealPath("");
        String uploadFilePath = applicationPath + File.separator + "upload";
        Part filePart;
        String fileName;
        InputStream fileContent;
        FileOutputStream fileOut;
        File fileSaveDir = new File(uploadFilePath);
        int buffer;
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdirs();
        }
        filePart = request.getPart("file");
//        fileName = usuario.getLogin()+".jpg";
        fileContent = filePart.getInputStream();
//        fileOut = new FileOutputStream(fileSaveDir+File.separator+fileName);
        fileOut = new FileOutputStream(fileSaveDir+File.separator+usuario.getLogin()+"_"+getFileName(filePart));
        while((buffer = fileContent.read()) > -1){
            fileOut.write(buffer);
        }
        fileContent.close();
        fileOut.close();

//        usuario.setFoto(fileName);
    }

}
