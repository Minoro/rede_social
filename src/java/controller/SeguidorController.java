package controller;

import dao.DAO;
import dao.DAOFactory;
import dao.SeguidorDAO;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Seguidor;
import model.Usuario;

@WebServlet(urlPatterns = {"/seguir",
                            "/nao_seguir"})
public class SeguidorController extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAO dao;
        RequestDispatcher dispatcher;
        
        switch(request.getServletPath()){
            case "/seguir":
                try(DAOFactory daoFactory = new DAOFactory();){
                    dao = daoFactory.getSeguidorDAO();
                    
                    HttpSession session = request.getSession(false);
                    Usuario usuario = (Usuario) session.getAttribute("usuario");
                    
                    Seguidor seguidor = new Seguidor();
                    seguidor.setId_seguido(Integer.parseInt(request.getParameter("id_seguido")));
                    seguidor.setId_seguidor(usuario.getId());
                    
                    dao.create(seguidor);
                    session.setAttribute("seguidor", seguidor);
//                    request.setAttribute("seguidor", seguidor);
                    
                    response.sendRedirect(request.getContextPath()+ "/post/post_seguidos");
//                    dispatcher.forward(request, response);
                    
                }catch(SQLException e){
                    System.out.println(e);
                }
            
                break;
            case "/nao_seguir":
                try(DAOFactory daoFactory = new DAOFactory();){
                    SeguidorDAO seguidorDAO = daoFactory.getSeguidorDAO();
                    
                    HttpSession session = request.getSession(false);
                    Usuario usuario = (Usuario) session.getAttribute("usuario");
                    
                    Seguidor seguidor = new Seguidor();
                    seguidor.setId_seguido(Integer.parseInt(request.getParameter("id_seguido")));
                    seguidor.setId_seguidor(usuario.getId());
                    
                    seguidorDAO.delete(seguidor);
                    session.setAttribute("seguidor", seguidor);
//                    request.setAttribute("seguidor", seguidor);
                    
                    response.sendRedirect(request.getContextPath()+ "/usuario");
//                    dispatcher.forward(request, response);
                    
                }catch(SQLException e){
                    System.out.println(e);
                }
                
                
                break;
             
        }
        
    } 
    
}
