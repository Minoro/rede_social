package model;

public class Seguidor {
    private Integer id_seguido;
    private Integer id_seguidor;

    public Integer getId_seguido() {
        return id_seguido;
    }

    public void setId_seguido(Integer id_seguido) {
        this.id_seguido = id_seguido;
    }

    public Integer getId_seguidor() {
        return id_seguidor;
    }

    public void setId_seguidor(Integer id_seguidor) {
        this.id_seguidor = id_seguidor;
    }
    
    
}
