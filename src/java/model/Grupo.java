package model;

public class Grupo {
    private Integer id;
    private String nome;
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    private Integer id_proprietario;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId_proprietario() {
        return id_proprietario;
    }

    public void setId_proprietario(Integer id_proprietario) {
        this.id_proprietario = id_proprietario;
    }
    
    
}
