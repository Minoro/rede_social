package model;

import java.sql.Timestamp;
import java.util.Date;

public class Republicacao {
    private Integer id_post;
    private Integer id_usuario_republicando;
    private Timestamp data_republicacao;

    public Timestamp getData_republicacao() {
        return data_republicacao;
    }

    public void setData_republicacao(Timestamp data_republicacao) {
        this.data_republicacao = data_republicacao;
    }
    
    public Integer getId_post() {
        return id_post;
    }

    public void setId_post(Integer id_post) {
        this.id_post = id_post;
    }

    public Integer getId_usuario_republicando() {
        return id_usuario_republicando;
    }

    public void setId_usuario_republicando(Integer id_usuario_republicando) {
        this.id_usuario_republicando = id_usuario_republicando;
    } 
}
