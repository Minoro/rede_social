package model;

import java.sql.Timestamp;
import java.util.Date;

public class Curtir {
   public final boolean CURTIR = true;
   public final boolean NAO_CURTIR = false;
   private Integer id_post;
   private Integer id_usuario;
   private boolean tipo;
   private Timestamp data_curtir;

    public Timestamp getData_curtir() {
        return data_curtir;
    }

    public void setData_curtir(Timestamp data_curtir) {
        this.data_curtir = data_curtir;
    }

    public boolean getTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }
   
    public Integer getId_post() {
        return id_post;
    }

    public void setId_post(Integer id_post) {
        this.id_post = id_post;
    }

    public Integer getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Integer id_usuario) {
        this.id_usuario = id_usuario;
    }
   
   
   
}
