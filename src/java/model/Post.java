package model;

import java.util.Date;
import java.sql.Timestamp;
        
public class Post{
    private Integer id;
    private String texto;
    private String titulo;
    private Integer id_autor;
    private Timestamp data_postagem;
    private Timestamp data_atualizacao_postagem;
    
    public Integer getId() {
        return id;
    }

    public Timestamp getData_postagem() {
        return data_postagem;
    }

    public void setData_postagem(Timestamp data_postagem) {
        this.data_postagem = data_postagem;
    }

    public Timestamp getData_atualizacao_postagem() {
        return data_atualizacao_postagem;
    }

    public void setData_atualizacao_postagem(Timestamp data_atualizacao_postagem) {
        this.data_atualizacao_postagem = data_atualizacao_postagem;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Integer getId_autor() {
        return id_autor;
    }

    public void setId_autor(Integer id_autor) {
        this.id_autor = id_autor;
    }
    
    
    
}
