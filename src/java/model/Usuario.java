package model;

import java.sql.Date;
import java.sql.Timestamp;

public class Usuario {
    private Integer id;
    private String nome;
    private String login;
    private String senha;
    private String descricao;
    private Timestamp data_nascimento;
    private String foto;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Timestamp getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Timestamp data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
    
    
}
