package model;

public class UsuarioSimilar extends Usuario{
    float similaridade;

    public float getSimilaridade() {
        return similaridade;
    }

    public void setSimilaridade(float similaridade) {
        this.similaridade = similaridade;
    }
}
