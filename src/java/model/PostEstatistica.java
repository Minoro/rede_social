package model;

public class PostEstatistica extends Post {
    private int impacto;

    public int getImpacto() {
        return impacto;
    }

    public void setImpacto(int impacto) {
        this.impacto = impacto;
    }
}
