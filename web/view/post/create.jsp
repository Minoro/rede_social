<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Criar Post</title>
        <script>
            var context = "<c:out value="${pageContext.servletContext.contextPath}" />";
            var login = "<c:out value="${usuario.login}" />";
            
        </script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/dropzone.min.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/vendor/js/jquery-1.11.1.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/upload.js"></script>
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/css/dropzone.css">
        
    </head>
    <body>
        <h1>Meus Post!</h1>
        <c:import url="../menu.jsp"/>
        <h1>Postar Texto</h1>
        <hr/><br/>
        <form action="${pageContext.servletContext.contextPath}/post/create" method="POST">
            <input type="hidden" name="id_autor" value="${usuario.id}">
            <h2>Titulo</h2>
            <input type="text" name="titulo" width="40"><br/>
            <h2>Texto</h2>
            <textarea rows= "5" cols="80" name= "texto" id="texto"></textarea><br><br>
            <input type="submit" value="Postar">
        </form>
            
        <form id="arquivo" class="dropzone" enctype="multipart/form-data" action="${pageContext.servletContext.contextPath}/post/arquivo">

        </form> 
            
        <a href="${pageContext.servletContext.contextPath}/post">Voltar</a>
    </body>
</html>
