<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comentar</title>
    </head>
    <body>
        <h1>Comentario</h1>
        <c:import url="../menu.jsp"/>
        <h2>Post Original</h2><hr /><br />
        <h2><c:out value="${post.titulo}" /></h2><hr /> <br />
        <c:out value="${post.texto}" /><hr /> <br />
        <hr/><br/>
        <form action="${pageContext.servletContext.contextPath}/comentario/create" method="POST">
            <input type="hidden" name="id_autor" value="${usuario.id}">
            <input type="hidden" name="id_post" value="${post.id}">
            <h2>Texto</h2>
            <textarea rows= "5" cols="80" name= "texto" id="texto"></textarea><br><br>
            <input type="submit" value="Postar">
        </form>
        <a href="${pageContext.servletContext.contextPath}/post">Voltar</a>
    </body>
</html>
