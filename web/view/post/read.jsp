<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ler Post</title>
    </head>
    <body>
        <h1>Posts do Usuário <c:out value="${usuario.nome}"/></h1>
        <c:import url="../menu.jsp"/>

        <ul>
            <li>Titulo:<c:out value="${post.titulo}"/></li>
            <li>Texto: <c:out value="${post.texto}"/></li>
            <li><a href="${pageContext.servletContext.contextPath}/curtir">Curtir</a></li>
        </ul>
            
        <h1><a href="${pageContext.servletContext.contextPath}/usuario">Voltar</a></h1>
    </body>
</html>
