<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Perfil</title>
    </head>
    <body>
        <h1>Pagina de Posts - Perfil</h1>
        <c:import url="../menu.jsp"/>
        <p>
            <img src="${pageContext.servletContext.contextPath}/foto_usuario/${usuario.foto}" width="250" height="200" />
        </p>
        <ul>
            <!--<li>ID: <c:out value="${usuario.id}"/></li>-->
            <li>Nome: <c:out value="${usuario.nome}"/></li>
            <li>Login: <c:out value="${usuario.login}"/></li>
            <li>Descricao:<c:out value="${usuario.descricao}"/></li>
            <li>Data de nascimento: <c:out value="${usuario.data_nascimento}"/></li>
        </ul>
        <hr />
        <table>
            <tr>
                <td><a href="${pageContext.servletContext.contextPath}/post/create">Criar Post</a></td>
                <td><a href="${pageContext.servletContext.contextPath}/post/minhas_postagens">Ver Minhas Postagens</a></td>
                <td><a href="${pageContext.servletContext.contextPath}/post/post_seguidos">Post dos Seguidos</a></td>
                <td>
                    <form action="${pageContext.servletContext.contextPath}/post/buscar" method="POST">
                        <label>Buscar Post: </label>
                        <input type="text" name="campo_busca">
                        <input type="submit" value="buscar">
                    </form>
                </td>
            </tr>
        </table>
        
        <hr />
        <a href="${pageContext.servletContext.contextPath}/usuario">Voltar</a>
        
        
    </body>
</html>
