<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>Resultado da Busca</title>
    </head>
    <body>
        <h1>Resultado Busca</h1>
        <c:import url = "../menu.jsp" />
        <form action="${pageContext.servletContext.contextPath}/post/buscar" method="POST">
            <label>Buscar: </label>
            <input type="text" name="campo_busca">
            <input type="submit" value="buscar">
        </form>
        <table>
                <c:forEach items="${postList}" varStatus="loop">
                <tr>
                    <td>
                        <h2><c:out value="${postList[loop.index].titulo}"/></h2><br />
                        <c:out value="${postList[loop.index].texto}" escapeXml="false"/> <hr />

                        <c:out value="Curtidas = ${curtidas[loop.index]}"/>
                        <c:out value="Não Curtidas = ${naoCurtidas[loop.index]}"/>

                        
                        <c:if test="${postList[loop.index].id_autor != usuario.id}">
                            <c:choose>
                                <c:when test="${statusCurtir[loop.index] == 0}">
                                    <a href="${pageContext.servletContext.contextPath}/curtir?id_post=${postList[loop.index].id}">Curtir</a>
                                    <a href="${pageContext.servletContext.contextPath}/nao_curtir?id_post=${postList[loop.index].id}">Não Curtir</a>
                                </c:when>
                                <c:when test="${statusCurtir[loop.index] == 1}">
                                    <a href="${pageContext.servletContext.contextPath}/desfazer_curtir?id_post=${postList[loop.index].id}">Desfazer Curtir</a>
                                    <a href="${pageContext.servletContext.contextPath}/inverter_curtir?id_post=${postList[loop.index].id}">Não Curtir</a>
                                </c:when>
                                <c:when test="${statusCurtir[loop.index] == 2}">
                                    <a href="${pageContext.servletContext.contextPath}/desfazer_curtir?id_post=${postList[loop.index].id}">Desfazer Não Curtir</a>
                                    <a href="${pageContext.servletContext.contextPath}/inverter_curtir?id_post=${postList[loop.index].id}">Curtir</a>
                                </c:when>

                            </c:choose>                   
                        </c:if>
                                    
                        <c:if test="${not empty comentarioList[loop.index]}">
                            <dl>
                                <dt><h3>Comentarios</h3></dl>
                        
                                <c:forEach  var="comentario" items="${comentarioList[loop.index]}">
                                    <dd>
                                        <c:out value="${comentario.texto}" escapeXml="false"/>
                                        <c:if test="${comentario.id_autor == usuario.id}">
                                            <form action="${pageContext.servletContext.contextPath}/post/update" method="POST">
                                                <input type="hidden" name="id" value="${comentario.id}"/>
                                                <input type="submit" value="Editar"/>
                                            </form>
                                        
                                            <form action="${pageContext.servletContext.contextPath}/post/delete" method="POST">
                                                <input type="hidden" name="id" value="${comentario.id}"/>
                                                <input type="submit" value="Excluir"/>
                                            </form>
                                        </c:if>
                                    </dd><hr /><br />
                                </c:forEach>
                                    
                            </dl>
                        </c:if>
                        
                        <!--<td><a href="${pageContext.servletContext.contextPath}/post/update?id=${p.id}">Editar</a></td>-->
                        <a href="${pageContext.servletContext.contextPath}/comentar?id_post=${postList[loop.index].id}">Comentar</a>
                        <c:if test="${postList[index].id_autor == usuario.id}">
                            <form action="${pageContext.servletContext.contextPath}/post/update" method="POST">
    <!--                        <td><a href="${pageContext.servletContext.contextPath}/post/editar">Editar</a></td>-->
                                <input type="hidden" name="id" value="${postList[loop.index].id}"/>
                                <td><input type="submit" value="Editar"/></td>
                            </form>
                            <form action="${pageContext.servletContext.contextPath}/post/delete" method="POST">
                                <!--<td><a href="${pageContext.servletContext.contextPath}/post/editar">Editar</a></td>-->
                                <input type="hidden" name="id" value="${postList[loop.index].id}"/>
                                <td><input type="submit" value="Excluir"/></td>
                            </form>
                        </c:if>
                        <c:if test="${postList[loop.index].id_autor != usuario.id}">
                            <form action="${pageContext.servletContext.contextPath}/republicar/delete" method="POST">
                                <!--<td><a href="${pageContext.servletContext.contextPath}/post/editar">Editar</a></td>-->
                                <input type="hidden" name="id" value="${postList[loop.index].id}"/>
                                <input type="hidden" name="id_usuario" value="${usuario.id}">
                                <td><input type="submit" value="Excluir"/></td>
                            </form>
                        </c:if>

                        
                    </td>
                </tr>
                
            </c:forEach>
        </table>
        
        <br /><br />
        <a href="${pageContext.servletContext.contextPath}/">Voltar</a>
    </body>
</html>