<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Post</title>
    </head>
    <body>
        <h1>Editar Post</h1>
        <c:import url="../menu.jsp"/>
        <form action="${pageContext.servletContext.contextPath}/post/update" method="POST">
            <input type="hidden" name="id" value="${post.id}">
            <input type="text" name="titulo" width="40" value="${post.titulo}"><br/>
            <input type="hidden" name="id_autor" value="${post.id_autor}">
            <textarea rows= "5" cols="80" name= "texto" id="texto">${post.texto}</textarea><br><br>
            <input type="submit" value="Postar">
        </form>
        <a href="${pageContext.servletContext.contextPath}/post">Voltar</a>
    </body>
</html>
