<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>Convidar Usuários</title>
    </head>
    <body>
        <h1>Lista de usuários</h1>
        <c:import url = "../menu.jsp" />
        <c:if test="${usuario.id == grupo.id_proprietario}">
            <form action="${pageContext.servletContext.contextPath}/grupo/convidar" method="POST">    
                <table>
                    <thead>
                        <tr>
                            <th>Login</th>
                        </tr>
                    </thead>
                        <tbody>
                            <c:forEach var="u" items="${usuarioList}">
                                <tr>
                                    <td>
                                        <a href="${pageContext.servletContext.contextPath}/usuario/read?id=${u.id}">
                                            <c:out value="${u.login}"/>
                                        </a>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="convidar" value="${u.id}">
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                </table>

                <input type="hidden" name="id_grupo" value="${grupo.id}">
                <c:if test="${not empty usuarioList}">
                    <input type="submit" value="Convidar Usuários">
                </c:if>    
            </form>
        </c:if>    
        <br /><br />
        <a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a>
    </body>
</html>