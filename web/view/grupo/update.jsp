<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Grupo</title>
    </head>
    <body>
        <h1>Editar Grupo</h1>
        <c:import url = "../menu.jsp" /> 
        <form action="${pageContext.servletContext.contextPath}/grupo/editar" method="POST">
            <input type="hidden" name="id" value="${grupo.id}">
            <input type="hidden" name="id_proprietario" value="${grupo.id_proprietario}">
            <h2>Nome</h2>
            <input type="text" name="nome" width="40" value="${grupo.nome}"><br/>
            <h2>Descrição</h2>
            <textarea rows= "5" cols="80" name= "descricao" id="descricao">${grupo.descricao}</textarea><br><br>
            <input type="submit" value="Editar Grupo">
        </form>
        <a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a>
        
    </body>
</html>
