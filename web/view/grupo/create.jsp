<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Criar Grupo</title>
    </head>
    <body>
        <h1>Criar Grupo</h1>
        <c:import url="../menu.jsp"/>
        <br/>
        
        <form action="${pageContext.servletContext.contextPath}/grupo/create" method="POST">
            <input type="hidden" name="id_proprietario" value="${usuario.id}">
            <h2>Nome do Grupo</h2>
            <input type="text" name="nome" width="40"><br/>
            <h2>Drescrição</h2>
            <textarea rows= "5" cols="80" name= "descricao" id="texto"></textarea><br><br>
            <input type="submit" value="Criar Grupo">
        </form>
        <br />
        <a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a>
    </body>
</html>
