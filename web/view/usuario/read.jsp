<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>Social</title>
    </head>
    <body>
        <h1>Visualização de Perfil</h1>
        <c:import url="../menu.jsp"/>
        <ul>
            <p>
            <img src="${pageContext.servletContext.contextPath}/foto_usuario/${u.foto}" width="250" height="200" />
            </p>
            <h2>Perfil do usuário ${u.nome}</h2>
            <li>Login: <c:out value="${u.login}"/></li>
            <li>Nome: <c:out value="${u.nome}"/></li>
            <li>Descricao <c:out value="${u.descricao}"/></li>
            <li>Data de nascimento: <c:out value="${u.data_nascimento}"/></li>
        </ul>
    
        <c:forEach items="${postList}" varStatus="loop">
            <h2> <c:out value="${postList[loop.index].titulo}"/> </h2>
            <h4> Autor - <c:out value="${postList[loop.index].nome}"/> </h4><br />
            
            <c:out value="${postList[loop.index].texto}" escapeXml="false"/><br />
            <c:out value="Curtidas = ${curtidas[loop.index]}"/>
            <c:out value="Não Curtidas = ${naoCurtidas[loop.index]}"/>
            
            <c:if test="${statusRepublicacao[loop.index] == 0 && postList[loop.index].id_autor != usuario.id}">
                <a href="${pageContext.servletContext.contextPath}/republicar?id_post=${postList[loop.index].id}">Republicar</a>
            </c:if>        
            <br />
            
            <c:choose>
                <c:when test="${statusCurtir[loop.index] == 0}">
                    <a href="${pageContext.servletContext.contextPath}/curtir?id_post=${postList[loop.index].id}">Curtir</a>
                    <a href="${pageContext.servletContext.contextPath}/nao_curtir?id_post=${postList[loop.index].id}">Não Curtir</a>
                </c:when>
                <c:when test="${statusCurtir[loop.index] == 1}">
                    <a href="${pageContext.servletContext.contextPath}/desfazer_curtir?id_post=${postList[loop.index].id}">Desfazer Curtir</a>
                    <a href="${pageContext.servletContext.contextPath}/inverter_curtir?id_post=${postList[loop.index].id}">Não Curtir</a>
                </c:when>
                <c:when test="${statusCurtir[loop.index] == 2}">
                    <a href="${pageContext.servletContext.contextPath}/desfazer_curtir?id_post=${postList[loop.index].id}">Desfazer Não Curtir</a>
                    <a href="${pageContext.servletContext.contextPath}/inverter_curtir?id_post=${postList[loop.index].id}">Curtir</a>
                </c:when>
                
            </c:choose>
            
            <a href="${pageContext.servletContext.contextPath}/comentar?id_post=${postList[loop.index].id}">Comentar</a>
                    <c:if test="${not empty comentarioList[loop.index]}">
                        <dl>
                            <dt><h3>Comentarios</h3></dl>
                            <c:forEach  var="comentario" items="${comentarioList[loop.index]}">
                                <dd>
                                    <c:out value="${comentario.texto}" escapeXml="false"/>
                                    <c:if test="${comentario.id_autor == usuario.id}">
                                        <form action="${pageContext.servletContext.contextPath}/post/update" method="POST">
                                            <input type="hidden" name="id" value="${comentario.id}"/>
                                            <input type="submit" value="Editar"/>
                                        </form>
                                        <form action="${pageContext.servletContext.contextPath}/post/delete" method="POST">
                                            <input type="hidden" name="id" value="${comentario.id}"/>
                                            <input type="submit" value="Excluir"/>
                                        </form>
                                    </c:if>
                                </dd><hr /><br />
                            </c:forEach>
                                    
                        </dl>
                    </c:if>        
                    
                    
        </c:forEach>
        
            
        <h1><a href="${pageContext.servletContext.contextPath}/welcome.jsp">Voltar</a></h1>
    </body>
</html>