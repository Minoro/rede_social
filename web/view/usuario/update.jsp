<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>Social Usuários</title>
    </head>
    <body>
        <h1>Edição do usuário <c:out value="${usuario.nome}"/></h1>
        <c:import url = "../menu.jsp" />  
        <form action="${pageContext.servletContext.contextPath}/usuario/update" method="POST" enctype="multipart/form-data">
<!--            <label>ID:</label><br>
            <input type="text" name="id_disabled" value="${usuario.id}" disabled><br><br>
            -->
            <label>Nome:</label><br>
            <input type="text" name="nome" value="${usuario.nome}"><br><br>
            
            <label>Login:</label><br>
            <input type="text" name="login" value="${usuario.login}"><br><br>

            <label>Senha:</label><br>
            <input type="password" name="senha"><br><br>
            
            <label>Descricao:</label><br>
            <textarea rows= "5" cols="80" name= "descricao" id="descricao">${usuario.descricao}</textarea><br><br>

            <label>Data de nascimento:</label><br>
            <input type="date" name="data_nascimento" value="${usuario.data_nascimento}"><br><br>
            
            <label>Foto:</label><br>
            <input type="file" name="foto"><br><br>
            
            <input type="hidden" name="foto_antiga" value="${usuario.foto}">
            <input type="hidden" name="id" value="${usuario.id}">

            <input type="submit" value="Enviar">
        </form>

        <h1><a href="${pageContext.servletContext.contextPath}/welcome.jsp  ">Voltar</a></h1>
    </body>
</html>