<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>Usuários</title>
    </head>
    <body>
        <h1>Lista de usuários</h1>
        <c:import url = "../menu.jsp" />
        <form action="${pageContext.servletContext.contextPath}/usuario/buscar" method="GET">
            <label>Buscar: </label>
            <input type="text" name="usuario_buscado">
            <input type="submit" value="buscar">
        </form>
        <table>
                <tbody>
                <th>Seguidos</th>
                    <c:forEach var="u" items="${seguidosList}">
                        <tr>
                            <td>
                                <a href="${pageContext.servletContext.contextPath}/usuario/read?id=${u.id}">
                                    <c:out value="${u.login} "/>
                                </a>
                            </td>
                            <td>
                                <a href="${pageContext.servletContext.contextPath}/estatistica/zona_influencia?id=${u.id}">
                                    Influência
                                </a>
                            </td>

                            <td>
                                <c:if test="${usuario.id != u.id}">
                                <a href="${pageContext.servletContext.contextPath}/nao_seguir?id_seguido=${u.id}">Não Seguir</a>
                                </c:if>
                                
                            </td>
                            
                        </tr>
                    </c:forEach>
                  
               <th>Outros Usuarios</th>
                    <c:forEach var="us" items="${naoSeguidosList}">
                        <tr>
                            <td>
                                <a href="${pageContext.servletContext.contextPath}/usuario/read?id=${us.id}">
                                    <c:out value="${us.login}"/>
                                </a>
                            </td>
                            <td>
                                <a href="${pageContext.servletContext.contextPath}/estatistica/zona_influencia?id=${us.id}">
                                    Influência
                                </a>
                            </td>

                            <td>
                                <c:if test="${usuario.id != us.id}">
                                <a href="${pageContext.servletContext.contextPath}/seguir?id_seguido=${us.id}">Seguir</a>
                                </c:if>
                                
                            </td>
                            
                        </tr>
                    </c:forEach>
                
                </tbody>
        </table>
        
        <br /><br />
        <a href="${pageContext.servletContext.contextPath}/">Voltar</a>
    </body>
</html>