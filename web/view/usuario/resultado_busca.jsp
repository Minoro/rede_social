<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>Usuários</title>
    </head>
    <body>
        <h1>Lista de usuários</h1>
        <c:import url = "../menu.jsp" />
        <form action="${pageContext.servletContext.contextPath}/usuario/buscar" method="GET">
            <label>Buscar: </label>
            <input type="text" name="usuario_buscado">
            <input type="submit" value="buscar">
        </form>
        <table>
                <tbody>
                <th>Resultado</th>
                    <%--<c:forEach var="u" items="${usuarioList}">--%>
                        <!--<tr>-->
                            <!--<td>-->
                                <!--<a href="${pageContext.servletContext.contextPath}/usuario/read?id=${u.id}">-->
                                    <%--<c:out value="${u.login}"/>--%>
                                <!--</a>-->
                            <!--</td>-->
                            
                        <!--</tr>-->
                    <%--</c:forEach>--%>
                
                    
                    <c:forEach items="${usuarioList}" varStatus="loop">
                        <c:if test="${usuarioList[loop.index].id != usuario.id}">
                            <tr>
                                <td><c:out value="${usuarioList[loop.index].login}"/></td>

                                <c:choose>
                                <c:when test="${estaSeguindoList[loop.index] == 0}">
                                    <td> <a href="${pageContext.servletContext.contextPath}/seguir?id_seguido=${usuarioList[loop.index].id}">Seguir</a></td>
                                </c:when>
                                <c:otherwise>
                                    <td> <a href="${pageContext.servletContext.contextPath}/nao_seguir?id_seguido=${usuarioList[loop.index].id}">Não Seguir</a></td>
                                </c:otherwise>
                                </c:choose>
                            </tr>
                        </c:if>
                    </c:forEach>
                </tbody>
        </table>
        
        <br /><br />
        <a href="${pageContext.servletContext.contextPath}/">Voltar</a>
    </body>
</html>