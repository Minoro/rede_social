<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estatisticas</title>
    </head>
    <body>
        <h1>Estatisticas</h1>
        <c:import url = "../menu.jsp" />
        <a href="${pageContext.servletContext.contextPath}/top20">TOP 20 Usuários</a>
        <a href="${pageContext.servletContext.contextPath}/top20_post">TOP 20 Posts</a>
        <a href="${pageContext.servletContext.contextPath}/similaridade?id=${usuario.id}">Usuários Similares</a>
        <a href="${pageContext.servletContext.contextPath}/flutuancia">Flutuancia</a>
    </body>
</html>
