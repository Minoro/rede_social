<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista dos Top20</title>
    </head>
    <body>
        <h1>TOP 20 Influências</h1>
        <c:import url = "../menu.jsp" />
        <form action="${pageContext.servletContext.contextPath}/estatisticas/top20" method="POST">
            <input type="hidden" name="id_proprietario" value="${usuario.id}">
            <label>Data de inicio: </label>
            <input type="date" name="data_inicio"> <br><br>
            
            <label>Data fim:</label>
            <input type="date" name="data_fim"> <br><br>
            
            <label>Hora Inicio (HH:mm): </label>
            <input type="time" name="hora_inicio"><br><br>
            
            <label>Hora Fim (HH:mm): </label>
            <input type="time" name="hora_fim"><br><br>
            
            <input type="submit" value="Listar">
            
        </form>
        <br />
        
        
    </body>
</html>
