<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista dos TOP20 Posts</title>
    </head>
    <body>
        <h1>Lista dos top20 Posts</h1>
        <c:import url = "../menu.jsp" />
            
        <table>
            <c:forEach var="p" items="${postsTop20}">
                <tr>
                    <td>
                        <h2><c:out value="${p.titulo}" /></h2>
                        <c:out value="${p.texto}"  escapeXml="false"/> <hr />
                    </td>
                    <td>
                        <c:out value="${p.impacto}"/>
                    </td>
                <tr>
            </c:forEach>
        <table>
            
    </body>
</html>
