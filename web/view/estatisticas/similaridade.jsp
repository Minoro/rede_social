<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Usuários Similares</title>
    </head>
    <body>
        <h1>10 Usuários mais próximos</h1>
        <c:import url = "../menu.jsp" />
        <table>
            <c:forEach var="u" items="${usuarios}">
                <tr>
                    <td>
                        <a href="${pageContext.servletContext.contextPath}/usuario/read?id=${u.id}">
                            <c:out value="${u.login}"/>
                        </a>
                    </td>
                    <td>
                        <c:out value="${u.similaridade}"/>
                    </td>

                </tr>
            </c:forEach>
        </table>
    </body>
</html>
