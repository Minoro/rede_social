<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lista dos TOP20</title>
    </head>
    <body>
        <h1>Lista dos top20</h1>
        <c:import url = "../menu.jsp" />
            
        <ul>
            <p>
            <img src="${pageContext.servletContext.contextPath}/foto_usuario/${usuario.foto}" width="250" height="200" />
            </p>
            <h2>Perfil do usuário ${usuario.nome}</h2>
            <li>Login: <c:out value="${usuario.login}"/></li>
            <li>Nome: <c:out value="${usuario.nome}"/></li>
            <li>Descricao <c:out value="${usuario.descricao}"/></li>
            <li>Data de nascimento: <c:out value="${usuario.data_nascimento}"/></li>
            <li>Zona de Influência: <c:out value="${zona_influencia}"/></li>
        </ul>
            
    </body>
</html>
