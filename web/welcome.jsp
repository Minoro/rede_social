<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <title>Social Início</title>
    </head>
    <body>
        <h1>Bem-vindo, <c:out value="${usuario.nome}"/>!</h1>
        <hr/>
        
        <table border="1">
            <tr>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/post">Perfil</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/grupo">Grupos</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario/update">Editar</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario/read">Dados</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario/delete">Excluir</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario">Usuários</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/estatisticas">Estatisticas</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/logout">Logout</a>
                </td>
                
            
            </tr>
        </table>
        <hr />
    </body>
</html>