<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Social Login</title>
        <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/assets/vendor/css/bootstrap.css">
    </head>
    <body>
        <form action="${pageContext.servletContext.contextPath}/login" method="POST">
            <label>Login:</label><br>
            <input type="text" name="login"><br><br>

            <label>Senha:</label><br>
            <input type="password" name="senha"><br><br>

            <input type="submit" value="Login"><br><br><br>
            
        </form>
            
        <a href="${pageContext.servletContext.contextPath}/usuario/create">Cadastrar novo usuário</a>
        
        
        <script src="${pageContext.servletContext.contextPath}/assets/js/bootstrap.js"></script>
        <script src="${pageContext.servletContext.contextPath}/assets/js/jquery-1.11.1.js"></script>
    </body>
</html>