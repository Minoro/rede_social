<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Meus Grupos</title>
    </head>
    <body>
        <h1>Meus Grupos</h1>
        <c:import url="../menu.jsp"/>
        <table>
            <c:forEach var="g" items="${grupoParticipaList}">
                <tr>
                    <td><a href="${pageContext.servletContext.contextPath}/grupo/read?id=${g.id}"><c:out value="${g.nome}"/></a></td>
                    <c:if test="${g.id_proprietario == usuario.id}">
                        
                        <form action="${pageContext.servletContext.contextPath}/grupo/convidar" method="GET">
                        <input type="hidden" name="id" value="${g.id}"/>
                        <td><input type="submit" value="Convidar Membros"/></td>
                        </form>
                        
                        <form action="${pageContext.servletContext.contextPath}/grupo/update" method="POST">
                        <input type="hidden" name="id" value="${g.id}"/>
                        <td><input type="submit" value="Editar"/></td>
                        </form>
                       
                        <form action="${pageContext.servletContext.contextPath}/grupo/delete" method="POST">
                            <input type="hidden" name="id" value="${g.id}"/>
                            <input type="hidden" name="id_usuario" value="${usuario.id}"/>
                            <td><input type="submit" value="Excluir"/></td>
                        </form>
                        
                    </c:if>
                </tr>
                
            </c:forEach>
        </table>
        <a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a>

        
    </body>
</html>
