<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grupo</title>
    </head>
    <body>
        <h1>Grupo <c:out value="${grupo.nome}"/></h1>
        <c:import url="../menu.jsp"/>
        <h2>Descrição:</h2>
        <hr />
        <c:out value="${grupo.descricao}"/>
        
        <h2>Membros:</h2>
        <form action="${pageContext.servletContext.contextPath}/grupo/remover_membro" method="POST">
            <table>
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Login</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach var="m" items="${membros}">
                        <tr>
                            <td><c:out value="${m.nome}"/></td>

                            <td><a href="${pageContext.servletContext.contextPath}/usuario/read?id=${m.id}">
                                    <c:out value="${m.login}"/>
                                </a>
                            </td>
                            <td>
                                <c:if test="${grupo.id_proprietario == usuario.id}">
                                    <c:if test="${m.id != usuario.id}">
                                        <input type="checkbox" name="delete" value="${m.id}">
                                    </c:if>
                                </c:if>
                            </td>
                        </tr>

                    </c:forEach>
                </tbody>
            </table>
            <c:if test="${grupo.id_proprietario == usuario.id}">
                <input type="hidden" name="id_grupo" value="${grupo.id}">
                <input type="submit" value="Remover do Grupo">
            </c:if>
        </form>        
        <br /><br />    
        <a href="${pageContext.servletContext.contextPath}/grupo">Voltar</a>
    </body>
</html>
