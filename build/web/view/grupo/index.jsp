<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grupos</title>
    </head>
    <body>
        <h1>Grupos</h1>
        <c:import url="../menu.jsp"/>
        
        <table>
            <tr>
                <td><a href="${pageContext.servletContext.contextPath}/grupo/create">Criar Grupo</a></td>
            </tr>
            <tr>
                <td><a href="${pageContext.servletContext.contextPath}/grupo/meus_grupos?id=${usuario.id}">Meus Grupos</a></td>    
            </tr>
            <tr>
                <td><a href="${pageContext.servletContext.contextPath}/grupo/todos">Todos Grupos</a></td>    
            </tr>
            
        </table>
         <br /><br />
        <a href="${pageContext.servletContext.contextPath}/">Voltar</a>
    </body>
</html>
