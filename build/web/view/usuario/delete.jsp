<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Deletar Usuário</title>
    </head>
    <body>
        <h1>Deletar Usuário</h1><br />
        <c:import url="../menu.jsp"/>
        Você deseja excluir seu cadastro?<br />
        <form action="${pageContext.servletContext.contextPath}/usuario/delete" method="POST">
            <input type="hidden" name="id" value="${usuario.id}">
            <input type="submit" value="Excluir Cadastro">
        </form>
        <!--<input name="" type="button" onClick="window.open('${pageContext.servletContext.contextPath}/welcome.jsp')" value="Voltar">-->
    
        <form action="${pageContext.servletContext.contextPath}/welcome.jsp">
            <input type="submit" value="Voltar">
        </form>
            
        <a href="${pageContext.servletContext.contextPath}/usuario">Voltar</a>
    </body>
</html>
