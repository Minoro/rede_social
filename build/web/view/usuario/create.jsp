<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>--%>
<!DOCTYPE html>
<html>
    <head>
        <title>Social Usuários</title>
    </head>
    <body>
        <h1>Cadastro de usuário</h1>

        <form action="${pageContext.servletContext.contextPath}/usuario/create" method="POST" enctype="multipart/form-data">
            <label>Nome:</label><br>
            <input type="text" name="nome"><br><br>
            
            <label>Login:</label><br>
            <input type="text" name="login"><br><br>

            <label>Senha:</label><br>
            <input type="password" name="senha"><br><br>

            <label>Descrição:</label><br>
            <textarea rows= "5" cols="80" name= "descricao" id="descricao"></textarea><br><br>
            
            <label>Data de nascimento:</label><br>
            <input type="date" name="data_nascimento"><br><br>
            
            <label>Foto:</label><br>
            <input type="file" name="foto"><br><br>
            
            <input type="submit" value="Enviar">
        </form>

        <a href="${pageContext.servletContext.contextPath}/usuario">Voltar</a>
    </body>
</html>