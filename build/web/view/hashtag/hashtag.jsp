<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hashtag</title>
    </head>
    <body>
        <h1>HASHTAG </h1>
        <c:import url="../menu.jsp"/>  
        <table>
            <c:forEach var="p" items="${posts}">
                <tr>
                    <td>
                    <h2><c:out value="${p.titulo}"/></h2><br />
                    <c:out value="${p.texto}" escapeXml="false"/> <hr />
                    </td>
                    
                    <!--<td><a href="${pageContext.servletContext.contextPath}/post/update?id=${p.id}">Editar</a></td>-->

                    <c:if test="${p.id_autor == usuario.id}">
                        <form action="${pageContext.servletContext.contextPath}/post/update" method="POST">
<!--                        <td><a href="${pageContext.servletContext.contextPath}/post/editar">Editar</a></td>-->
                            <input type="hidden" name="id" value="${p.id}"/>
                            <td><input type="submit" value="Editar"/></td>
                        </form>
                        <form action="${pageContext.servletContext.contextPath}/post/delete" method="POST">
                            <!--<td><a href="${pageContext.servletContext.contextPath}/post/editar">Editar</a></td>-->
                            <input type="hidden" name="id" value="${p.id}"/>
                            <td><input type="submit" value="Excluir"/></td>
                        </form>
                    </c:if>
                    
                </tr>
                
            </c:forEach>
        </table>
        <a href="${pageContext.servletContext.contextPath}/post">Voltar</a>
        
    </body>
</html>