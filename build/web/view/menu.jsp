<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Social</title>
    </head>
    <body>
         <hr/>
        
        <table border="1">
            <tr>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/post">Perfil</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/grupo">Grupos</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario/update">Editar</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario/read">Dados</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario/delete">Excluir</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/usuario">Usuários</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/estatisticas">Estatisticas</a>
                </td>
                <td>
                    <a href="${pageContext.servletContext.contextPath}/logout">Logout</a>
                </td>
                
            
            </tr>
        </table>
        <hr />
    </body>
</html>
