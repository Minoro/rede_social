<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${empty sessionScope.usuario}">
    <c:redirect context="${pageContext.servletContext.contextPath}" url="/"/>
</c:if>
<!DOCTYPE html>


<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Hora', '<c:out value="${usuarios[0].login}"/>', '<c:out value="${usuarios[1].login}"/>', '<c:out value="${usuarios[2].login}"/>'],
            
            <c:forEach items="${flutuacao1}" varStatus="loop">
                ['<c:out value="${loop.index}" />', <c:out value="${flutuacao1[loop.index]}"/>, <c:out value="${flutuacao2[loop.index]}" />, <c:out value="${flutuacao3[loop.index]}"/> ],
            </c:forEach>

        ]);

        var options = {
          title: 'Company Performance'
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
      <h1>Gráfico de flutuancia</h1>
    <c:import url = "../menu.jsp" />
    <div id="chart_div" style="width: 900px; height: 500px;"></div>
  </body>
</html>
